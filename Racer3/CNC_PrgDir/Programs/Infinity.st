VAR CONSTANT
    P : McPointType := (Pos:=(Y:=50, Z:=900));
END_VAR

PROGRAM _MAIN
    Feedrate(10000);
    PlaneXY();
    MoveCCWAngle(P, 360);     
END_PROGRAM