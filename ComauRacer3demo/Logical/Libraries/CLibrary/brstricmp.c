
#include <bur/plctypes.h>
#include "math.h"
//#include <cstring>
#include "string.h"

#ifdef __cplusplus
	extern "C"
	{
#endif
	#include "CLibrary.h"
#ifdef __cplusplus
	};
#endif
/* This function compares 2 strings (ignores upper and lower case) */
void brstricmp(struct brstricmp* inst)
{
	inst->result = !strcasecmp(inst->string1, inst->string2); // result is TRUE if strings are the same, otherwise it's FALSE.
	return;
	/*TODO: Add your code here*/
}
