
{REDUND_ERROR} FUNCTION_BLOCK brstricmp (*This function compares 2 strings (ignores upper and lower case)*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		string1 : {REDUND_UNREPLICABLE} STRING[80];
		string2 : {REDUND_UNREPLICABLE} STRING[80];
	END_VAR
	VAR_OUTPUT
		result : BOOL;
	END_VAR
END_FUNCTION_BLOCK
