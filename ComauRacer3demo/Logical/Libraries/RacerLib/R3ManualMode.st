
(* TODO: Add your comment here *)
FUNCTION_BLOCK R3ManualMode
	
	IF ManualModePara.Direction = 1 THEN // set actual axis velocity based on which direction it's set to be moved
		JogVelocityActual := ManualModePara.JogVelocity;
	ELSIF ManualModePara.Direction = -1 THEN
		JogVelocityActual := -1 * ManualModePara.JogVelocity;
	ELSIF  ManualModePara.Direction = 0 THEN
		JogVelocityActual := 0;
	END_IF
	
	RoboArmPara.Jog.CoordSystem := ManualModePara.CoordinateSystem;
	RoboArmPara.Jog.PathLimits := ManualModePara.PathLimits;
	
	IF ManualModePara.ActivateMove THEN
		
		IF ManualModePara.AxisButton.Q1 THEN
			RoboArmPara.Jog.Velocity[0] := JogVelocityActual;
		ELSE
			RoboArmPara.Jog.Velocity[0] := 0;
		END_IF
		
		IF ManualModePara.AxisButton.Q2 THEN
			RoboArmPara.Jog.Velocity[1] := JogVelocityActual;
		ELSE
			RoboArmPara.Jog.Velocity[1] := 0;
		END_IF
		
		IF ManualModePara.AxisButton.Q3 THEN
			RoboArmPara.Jog.Velocity[2] := JogVelocityActual;
		ELSE
			RoboArmPara.Jog.Velocity[2] := 0;
		END_IF
		
		IF ManualModePara.AxisButton.Q4 THEN
			RoboArmPara.Jog.Velocity[3] := JogVelocityActual;
		ELSE
			RoboArmPara.Jog.Velocity[3] := 0;
		END_IF
		
		IF ManualModePara.AxisButton.Q5 THEN
			RoboArmPara.Jog.Velocity[4] := JogVelocityActual;
		ELSE
			RoboArmPara.Jog.Velocity[4] := 0;
		END_IF
		
		IF ManualModePara.AxisButton.Q6 THEN
			RoboArmPara.Jog.Velocity[5] := JogVelocityActual;
		ELSE
			RoboArmPara.Jog.Velocity[5] := 0;
		END_IF
		
		IF RoboArm.Info.PLCopenState = mcGROUP_STANDBY THEN
			RoboArm.Jog := TRUE;
		ELSIF RoboArm.Info.PLCopenState = mcGROUP_STOPPING AND RoboArm.Stopped THEN
			RoboArm.Stop := FALSE;
		END_IF
		
	ELSIF RoboArm.Info.PLCopenState = mcGROUP_MOVING THEN
		RoboArm.Jog := FALSE;
		RoboArm.Stop := TRUE;
	ELSE
		RoboArm.Stop := FALSE;
	END_IF

	
	IF ManualModePara.ExitManual THEN
		//clean up
		ManualModePara.Direction := 0;
		JogVelocityActual := 0;
		FOR j := 0 TO 5 BY 1 DO //set every jog velocity to 0
				RoboArmPara.Jog.Velocity[j] := 0;
		END_FOR;
		ManualModePara.ActivateMove := FALSE;
		RoboArm.Jog := FALSE;
		RoboArm.Stop := FALSE;
		//go out
		ManualModePara.ExitManual := FALSE;
	END_IF
	
		
END_FUNCTION_BLOCK
