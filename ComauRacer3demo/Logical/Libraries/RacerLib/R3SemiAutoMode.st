
FUNCTION_BLOCK R3SemiAutoMode

	IF SemiAutoModePara.ExitSemiAuto THEN
		SemiAutoModePara.State := semiSTATE_STOP;
	END_IF
	
	(*
	IF SemiAutoModePara.Pause THEN
		SemiAutoModePara_memory := SemiAutoModePara;
		RoboArm.MoveDirect := FALSE;
		RoboArm.MoveLinear := FALSE;
		SemiAutoModePara.Flag := FALSE;
	END_IF
	*)
	
	IF SemiAutoModePara.CoordinateSystem = 9 THEN // TCS position read
		SemiAutoModePara.CurrentTCSPos[0] := RoboArm.X;
		SemiAutoModePara.CurrentTCSPos[1] := RoboArm.Y;
		SemiAutoModePara.CurrentTCSPos[2] := RoboArm.Z;
		SemiAutoModePara.CurrentTCSPos[3] := RoboArm.A;
		SemiAutoModePara.CurrentTCSPos[4] := RoboArm.B;
		SemiAutoModePara.CurrentTCSPos[5] := RoboArm.C;
	END_IF
		
	CASE SemiAutoModePara.State OF
		semiSTATE_INIT:
			
			SemiAutoModePara.MoveDone := FALSE;
			
			IF SemiAutoModePara.Flag THEN
				SemiAutoModePara.State := semiSTATE_UPDATE;
			END_IF

			//TODO CHECK AXIS LIMITS AND SINGULAR POINT AND SEND ERROR (ALARM) AND AVOID MOVING
			//IF checkAxisLimits(Q1 := SemiAutoModePara.AskedValue.Q0,Q2 := SemiAutoModePara.AskedValue.Q1,Q3 := SemiAutoModePara.AskedValue.Q2,Q4 := SemiAutoModePara.AskedValue.Q3,Q5 := SemiAutoModePara.AskedValue.Q4,Q6 := SemiAutoModePara.AskedValue.Q5,Mode := SemiAutoModePara.ModeForThisMove, RoboArm := RoboArm) THEN
			// send alarm out of bounds
			//MpAlarmXSet(gAlarmXCore, 'OutOfLimits');
			//END_IF
			
			// check for singular point and send alarm
			
			IF NOT SemiAutoModePara.Mode THEN //1 for relative, 0 for absolute - set axis limits
				SemiAutoModePara.ValueLimits.ACSHigh.Val1 := 170.0; //Q1
				SemiAutoModePara.ValueLimits.ACSLow.Val1 := -170.0;
				
				SemiAutoModePara.ValueLimits.ACSHigh.Val2 := 135.0; //Q2
				SemiAutoModePara.ValueLimits.ACSLow.Val2 := -95.0;
				
				SemiAutoModePara.ValueLimits.ACSHigh.Val3 := 90.0; //Q3
				SemiAutoModePara.ValueLimits.ACSLow.Val3 := -155.0;
				
				SemiAutoModePara.ValueLimits.ACSHigh.Val4 := 200.0; //Q4
				SemiAutoModePara.ValueLimits.ACSLow.Val4 := -200.0;
				
				SemiAutoModePara.ValueLimits.ACSHigh.Val5 := 105.0; //Q5
				SemiAutoModePara.ValueLimits.ACSLow.Val5 := -105.0;
				
				SemiAutoModePara.ValueLimits.ACSHigh.Val6 := 360.0; //Q6
				SemiAutoModePara.ValueLimits.ACSLow.Val6 := -360.0;
				
				SemiAutoModePara.ValueLimits.GCSHigh.Val1 := 920.0; //X
				SemiAutoModePara.ValueLimits.GCSLow.Val1 := 0.0;
				
				SemiAutoModePara.ValueLimits.GCSHigh.Val2 := 920.0; //Y
				SemiAutoModePara.ValueLimits.GCSLow.Val2 := 0.0;
				
				SemiAutoModePara.ValueLimits.GCSHigh.Val3 := 1300.0; //Z
				SemiAutoModePara.ValueLimits.GCSLow.Val3 := 0.0;
				
				SemiAutoModePara.ValueLimits.GCSHigh.Val4 := 180.0; //A
				SemiAutoModePara.ValueLimits.GCSLow.Val4 := -180.0;
				
				SemiAutoModePara.ValueLimits.GCSHigh.Val5 := 180.0; //B
				SemiAutoModePara.ValueLimits.GCSLow.Val5 := -180.0;
				
				SemiAutoModePara.ValueLimits.GCSHigh.Val6 := 180.0; //C
				SemiAutoModePara.ValueLimits.GCSLow.Val6 := -180.0;
			ELSE
				SemiAutoModePara.ValueLimits.ACSHigh.Val1 := 170.0 - RoboArm.Info.JointAxisPosition[0]; //Q1
				SemiAutoModePara.ValueLimits.ACSLow.Val1 := -170.0 - RoboArm.Info.JointAxisPosition[0];
				
				SemiAutoModePara.ValueLimits.ACSHigh.Val2 := 135.0 - RoboArm.Info.JointAxisPosition[1]; //Q2
				SemiAutoModePara.ValueLimits.ACSLow.Val2 := -95.0 - RoboArm.Info.JointAxisPosition[1];
				
				SemiAutoModePara.ValueLimits.ACSHigh.Val3 := 90.0 - RoboArm.Info.JointAxisPosition[2]; //Q3
				SemiAutoModePara.ValueLimits.ACSLow.Val3 := -155.0 - RoboArm.Info.JointAxisPosition[2];
				
				SemiAutoModePara.ValueLimits.ACSHigh.Val4 := 200.0 - RoboArm.Info.JointAxisPosition[3]; //Q4
				SemiAutoModePara.ValueLimits.ACSLow.Val4 := -200.0 - RoboArm.Info.JointAxisPosition[3];
				
				SemiAutoModePara.ValueLimits.ACSHigh.Val5 := 105.0 - RoboArm.Info.JointAxisPosition[4]; //Q5
				SemiAutoModePara.ValueLimits.ACSLow.Val5 := -105.0 - RoboArm.Info.JointAxisPosition[4];
				
				SemiAutoModePara.ValueLimits.ACSHigh.Val6 := 360.0 - RoboArm.Info.JointAxisPosition[5]; //Q6
				SemiAutoModePara.ValueLimits.ACSLow.Val6 := -360.0 - RoboArm.Info.JointAxisPosition[5];
				
				SemiAutoModePara.ValueLimits.GCSHigh.Val1 := 920.0 - RoboArm.X; //X
				SemiAutoModePara.ValueLimits.GCSLow.Val1 := 0.0 - RoboArm.X;
				
				SemiAutoModePara.ValueLimits.GCSHigh.Val2 := 920.0 - RoboArm.Y; //Y
				SemiAutoModePara.ValueLimits.GCSLow.Val2 := 0.0 - RoboArm.Y;
				
				SemiAutoModePara.ValueLimits.GCSHigh.Val3 := 1300.0 - RoboArm.Z; //Z
				SemiAutoModePara.ValueLimits.GCSLow.Val3 := 0.0 - RoboArm.Z;
				
				SemiAutoModePara.ValueLimits.GCSHigh.Val4 := 180.0 - RoboArm.A; //A
				SemiAutoModePara.ValueLimits.GCSLow.Val4 := -180.0 - RoboArm.A;
				
				SemiAutoModePara.ValueLimits.GCSHigh.Val5 := 180.0 - RoboArm.B; //B
				SemiAutoModePara.ValueLimits.GCSLow.Val5 := -180.0 - RoboArm.B;
				
				SemiAutoModePara.ValueLimits.GCSHigh.Val6 := 180.0 - RoboArm.C; //C
				SemiAutoModePara.ValueLimits.GCSLow.Val6 := -180.0 - RoboArm.C;
			END_IF
				
		semiSTATE_UPDATE:
			
			RoboArmPara.CoordSystem := SemiAutoModePara.CoordinateSystem;
			
			//RoboArmPara.Velocity := SemiAutoModePara.AskedValue.Velocity;
			//RoboArmPara.Acceleration := SemiAutoModePara.AskedValue.Acceleration;
			//RoboArmPara.Deceleration := SemiAutoModePara.AskedValue.Deceleration;
			//RoboArmPara.Jerk := SemiAutoModePara.AskedValue.Jerk;
			
			SemiAutoModePara.ModeForThisMove := SemiAutoModePara.Mode;
			SemiAutoModePara.PathModeForThisMove := SemiAutoModePara.PathMode;
			
			IF SemiAutoModePara.CoordinateSystem = 0 THEN // ACS
				
				IF SemiAutoModePara.ModeForThisMove THEN //relative
					RoboArmPara.Distance[0] := SemiAutoModePara.AskedValue.ACS[0];
					RoboArmPara.Distance[1] := SemiAutoModePara.AskedValue.ACS[1];
					RoboArmPara.Distance[2] := SemiAutoModePara.AskedValue.ACS[2];
					RoboArmPara.Distance[3] := SemiAutoModePara.AskedValue.ACS[3];
					RoboArmPara.Distance[4] := SemiAutoModePara.AskedValue.ACS[4];
					RoboArmPara.Distance[5] := SemiAutoModePara.AskedValue.ACS[5];
				ELSE //absolute
					RoboArmPara.Position[0] := SemiAutoModePara.AskedValue.ACS[0];
					RoboArmPara.Position[1] := SemiAutoModePara.AskedValue.ACS[1];
					RoboArmPara.Position[2] := SemiAutoModePara.AskedValue.ACS[2];
					RoboArmPara.Position[3] := SemiAutoModePara.AskedValue.ACS[3];
					RoboArmPara.Position[4] := SemiAutoModePara.AskedValue.ACS[4];
					RoboArmPara.Position[5] := SemiAutoModePara.AskedValue.ACS[5];
				END_IF
				
			ELSIF SemiAutoModePara.CoordinateSystem = 9 THEN // TCS
				(*SemiAutoModePara.GroupRead.Enable := TRUE;
				SemiAutoModePara.GroupRead.AxesGroup := RoboArm.MpLink;
				SemiAutoModePara.GroupRead.CoordSystem := 9;
				SemiAutoModePara.GroupRead.ValueSource := mcVALUE_ACTUAL;
				//SemiAutoModePara.GroupRead(); //disabled until the issue is resolved. seems complex
				
				IF SemiAutoModePara.GroupRead.Valid THEN
					SemiAutoModePara.CurrentTCSPos := SemiAutoModePara.GroupRead.CyclicPosition;
				ELSE
					SemiAutoModePara.CurrentTCSPos[0] := RoboArm.X;
					SemiAutoModePara.CurrentTCSPos[1] := RoboArm.Y;
					SemiAutoModePara.CurrentTCSPos[2] := RoboArm.Z;
					SemiAutoModePara.CurrentTCSPos[3] := RoboArm.A;
					SemiAutoModePara.CurrentTCSPos[4] := RoboArm.B;
					SemiAutoModePara.CurrentTCSPos[5] := RoboArm.C;
				END_IF*)
								
				IF SemiAutoModePara.ModeForThisMove THEN //relative
					RoboArmPara.Distance[0] := SemiAutoModePara.AskedValue.TCS[0];
					RoboArmPara.Distance[1] := SemiAutoModePara.AskedValue.TCS[1];
					RoboArmPara.Distance[2] := SemiAutoModePara.AskedValue.TCS[2];
					RoboArmPara.Distance[3] := SemiAutoModePara.AskedValue.TCS[3];
					RoboArmPara.Distance[4] := SemiAutoModePara.AskedValue.TCS[4];
					RoboArmPara.Distance[5] := SemiAutoModePara.AskedValue.TCS[5];
				ELSE //absolute
					RoboArmPara.Position[0] := SemiAutoModePara.AskedValue.TCS[0];
					RoboArmPara.Position[1] := SemiAutoModePara.AskedValue.TCS[1];
					RoboArmPara.Position[2] := SemiAutoModePara.AskedValue.TCS[2];
					RoboArmPara.Position[3] := SemiAutoModePara.AskedValue.TCS[3];
					RoboArmPara.Position[4] := SemiAutoModePara.AskedValue.TCS[4];
					RoboArmPara.Position[5] := SemiAutoModePara.AskedValue.TCS[5];
				END_IF
				
			ELSIF SemiAutoModePara.CoordinateSystem = 10 THEN // GCS
				
				IF SemiAutoModePara.ModeForThisMove THEN //relative
					RoboArmPara.Distance[0] := SemiAutoModePara.AskedValue.GCS[0];
					RoboArmPara.Distance[1] := SemiAutoModePara.AskedValue.GCS[1];
					RoboArmPara.Distance[2] := SemiAutoModePara.AskedValue.GCS[2];
					RoboArmPara.Distance[3] := SemiAutoModePara.AskedValue.GCS[3];
					RoboArmPara.Distance[4] := SemiAutoModePara.AskedValue.GCS[4];
					RoboArmPara.Distance[5] := SemiAutoModePara.AskedValue.GCS[5];
				ELSE //absolute
					RoboArmPara.Position[0] := SemiAutoModePara.AskedValue.GCS[0];
					RoboArmPara.Position[1] := SemiAutoModePara.AskedValue.GCS[1];
					RoboArmPara.Position[2] := SemiAutoModePara.AskedValue.GCS[2];
					RoboArmPara.Position[3] := SemiAutoModePara.AskedValue.GCS[3];
					RoboArmPara.Position[4] := SemiAutoModePara.AskedValue.GCS[4];
					RoboArmPara.Position[5] := SemiAutoModePara.AskedValue.GCS[5];
				END_IF
				
			END_IF
			
			IF SemiAutoModePara.Mode THEN
				RoboArmPara.ManualMoveType := 1;
			ELSE
				RoboArmPara.ManualMoveType := 0;
			END_IF
			
			RoboArm.Update := TRUE;
			
			IF RoboArm.UpdateDone THEN
				RoboArm.Update := FALSE;
				SemiAutoModePara.UpdatePending := FALSE;
				SemiAutoModePara.State := semiSTATE_START;
			END_IF
			
		semiSTATE_START:
			
			IF SemiAutoModePara.Flag THEN
				IF SemiAutoModePara.PathModeForThisMove THEN
					RoboArm.MoveLinear := TRUE;
				ELSE 
					RoboArm.MoveDirect := TRUE;
				END_IF
				SemiAutoModePara.State := semiSTATE_GO;
			ELSE
				SemiAutoModePara.State := semiSTATE_STOP;
			END_IF
			
			
		semiSTATE_GO:
			
			RoboArmPara.Velocity := SemiAutoModePara.AskedValue.Velocity;
			
			IF NOT SemiAutoModePara.Flag THEN
				SemiAutoModePara.State := semiSTATE_STOP;
			END_IF
			
			IF RoboArm.MoveDone THEN
				IF SemiAutoModePara.PathModeForThisMove THEN
					RoboArm.MoveLinear := FALSE;
				ELSE 
					RoboArm.MoveDirect := FALSE;
				END_IF
				SemiAutoModePara.State := semiSTATE_INIT;
				SemiAutoModePara.Flag := FALSE;
				SemiAutoModePara.MoveDone := TRUE;
			END_IF
			
		semiSTATE_STOP:
			
			RoboArm.MoveDirect := FALSE;
			RoboArm.MoveLinear := FALSE;
			SemiAutoModePara.Flag := FALSE;
			SemiAutoModePara.State := semiSTATE_INIT;
			SemiAutoModePara.ExitSemiAuto := FALSE;
			
		semiSTATE_PAUSE:
			//TODO count movement deegrees in relative and substract so unpause in relative mode "continues"

			
	END_CASE
END_FUNCTION_BLOCK
