(*Automatic mode*)

TYPE
	R3AutomaticModeInfo : 	STRUCT 
		CurrentState : R3AutomaticModeState; (*Current automode state*)
		Paused : BOOL; (*Tells the main program that a movement is paused*)
		Updated : BOOL; (*Tells the FB that the robot's parameters have been updated*)
		ClearStream : BOOL; (*Tells the main program to clear the svg stream*)
		PreviewFilePath : STRING[160]; (*/FileDevice:CNC_PrgDir/PathPreview.svg*)
		PreviewFileStatus : DINT; (*File preview status*)
		PreviewOverlay : STRING[160] := '<foreignObject x="0" y="0" width="470" height="470"><img src="Media/Coordsys.png" width="470" height="470"/></foreignObject>'; (*/FileDevice:CNC_PrgDir/PathPreview3D.svg*)
		Preview3DFileStatus : DINT;
		FileCopied : BOOL;
		LastProgram : STRING[260]; (*Last run program*)
		SimDone : BOOL; (*Program simulation done*)
		M17Counter : DINT; (*This variable is used for M17 function occurence counter*)
		M16Counter : DINT; (*This variable is used for M16 function occurence counter*)
		Show3D : BOOL; (*Used for visualisation*)
	END_STRUCT;
	R3AutomaticModePara : 	STRUCT  (*Automatic mode parameters*)
		ProgramName : STRING[260]; (*Program name to load/execute*)
		Execute : BOOL; (*Executes a program*)
		NOTExecute : BOOL; (*NOT Execute, only for button visibility*)
		ErrorReset : BOOL; (*Resets errors*)
		Abort : BOOL; (*Aborts execution.*)
		Pause : BOOL; (*Pauses execution*)
		Continue : BOOL; (*Continues execution*)
		Continuous : BOOL; (*Defines whether the program will be called continuously*)
		JustSim : BOOL; (*just simulate movement (for path preview)*)
		FileCopy : FileCopy;
		ToolOutputReset : BOOL; (*Tool output reset (M17)*)
		ToolOutput : BOOL; (*Tool output (M16)*)
	END_STRUCT;
	R3AutomaticModeState : 
		( (*Automatic mode state machine*)
		autoSTATE_IDLE := 1, (*waiting for instruction*)
		autoSTATE_EXECUTE := 4, (*executing program*)
		autoSTATE_DONE := 5, (*execution done*)
		autoSTATE_PAUSE := 6, (*program paused*)
		autoSTATE_CONTINUE := 7, (*Continues paused program*)
		autoSTATE_ABORT := 8 (*Abort current program*)
		);
END_TYPE

(*Calibration mode*)

TYPE
	R3Axis356MoveType : 	STRUCT  (*Type for storing axis 3 and 5 settings for axes 5 and 6 calibration*)
		Axis2Moved : BOOL; (*Stores whether axis 2 has been moved in order to calibrate axis 3*)
		Axis3Moved : BOOL; (*Stores whether axis 3 has been moved in order to calibrate axes 5 and  6*)
		Axis5Moved : BOOL; (*Stores whether axis 5 has been moved in order to calibrate axis 6*)
		AxisMoveVelocity : REAL; (*Velocity for predefined axis movements*)
		AxisAngles : ARRAY[0..5]OF LREAL := [0,45,-90,0,-118,0]; (*Array of axis positions used for easier calibration*)
		HomeReturn : BOOL; (*Flag to be set by a button to restore all axes after calibrating axis 6*)
		AxesRestored : BOOL; (*Axes have been restored to previous position*)
		MoveAxis : BOOL; (*This enables manual switch for predefined movemets used for calibration*)
		MoveEnable : BOOL; (*This enables the button for above action in HMI*)
	END_STRUCT;
	R3AxisBtnType : 	STRUCT  (*Global Coordinate System Select button*)
		Q1 : BOOL;
		Q2 : BOOL;
		Q3 : BOOL;
		Q4 : BOOL;
		Q5 : BOOL;
		Q6 : BOOL;
	END_STRUCT;
	R3CalibrationCmds : 	STRUCT 
		Mode : R3CalibrationMode;
		InitHome : MC_BR_InitHome_AcpAx;
		GroupHome : MC_BR_GroupHome_15;
		ProcessConfig : MC_BR_ProcessParam;
	END_STRUCT;
	R3CalibrationInfo : 	STRUCT 
		CurrentState : R3CalibrationOuterStateEnum;
		Axis5LimitsSet : BOOL;
		Axis5LimitsRead : BOOL;
	END_STRUCT;
	R3CalibrationMainType : 	STRUCT 
		Info : R3CalibrationInfo; (*Information portion of calibration.*)
		Cmds : R3CalibrationCmds; (*Calibration commands*)
		Para : R3CalibrationParaType; (*Calibration parameters*)
	END_STRUCT;
	R3CalibrationOuterStateEnum : 
		(
		STATE_BEGIN,
		STATE_FAKE_HOME,
		STATE_MOVING,
		STATE_SAVING_POSITION,
		STATE_DONE
		);
	R3CalibrationParaType : 	STRUCT 
		Parameters : R3CalibrationType;
		HomingParameters : McAcpAxHomingParType;
		FakeHomingModeEnum : ARRAY[0..14]OF McHomingModeEnum := [15(mcHOMING_DIRECT)];
		Positions : ARRAY[0..14]OF LREAL := [15(0.0)];
		HomingModeEnum : ARRAY[0..14]OF McHomingModeEnum := [15(mcHOMING_INIT)];
		AxisLim : McCfgAxMoveLimType;
	END_STRUCT;
	R3CalibrationStateEnum : 
		(
		STATE_START,
		STATE_CALIBRATE_Q1,
		STATE_CALIBRATE_Q2,
		STATE_CALIBRATE_Q3,
		STATE_CALIBRATE_Q4,
		STATE_CALIBRATE_Q5,
		STATE_CALIBRATE_Q6,
		STATE_RESTORE_HOME
		);
	R3CalibrationType : 	STRUCT 
		CalibrationState : R3CalibrationStateEnum; (*State machine used to control Calibration behaviour*)
		CalibrationModePara : R3ManualModeType; (*same type used for manual mode (slightly different code handling)*)
		AxisCalibrated : ARRAY[0..5]OF BOOL := [6(0)]; (*Saves which axes have been calibrated (defaults to 0 upon creation)*)
		AxisSaveBtn : BOOL; (*Button to save current position as calibrated*)
		Axis5To6Para : R3Axis356MoveType; (*Stores axis 2,  4 and 5 settings for axis 3, 5 and 6 calibration*)
		SavedPositions : ARRAY[0..5]OF LREAL;
		GroupHome : MC_BR_GroupHome_15;
		FakeHomingModeEnum : ARRAY[0..14]OF McHomingModeEnum := [15(mcHOMING_DIRECT)];
		Positions : ARRAY[0..14]OF LREAL := [15(0.0)];
		AxesGroupRef : UDINT;
		SaveEnable : BOOL; (*Defines the enable status of "Save axis" button in HMI.*)
	END_STRUCT;
END_TYPE

(*Brake setting*)

TYPE
	R3BrakeCmdsType : 	STRUCT 
		BrakeOperation : MC_BR_GroupBrakeOperation;
	END_STRUCT;
	R3BrakeInfoType : 	STRUCT 
		BrakeStatus : ARRAY[0..2]OF McBrakeStatusEnum;
		Done : BOOL;
		State : R3BrakeStateMachineEnum;
		Error : BOOL;
	END_STRUCT;
	R3BrakeParaType : 	STRUCT 
		BrakeCmd : McBrakeCmdEnum;
		Identifier : UDINT;
	END_STRUCT;
	R3BrakeStateMachineEnum : 
		(
		brakeREAD,
		brakeOPEN,
		brakeCLOSE,
		brakeDONE,
		brakeERROR
		);
	R3BrakeType : 	STRUCT 
		Parameters : R3BrakeParaType;
		Info : R3BrakeInfoType;
		Cmds : R3BrakeCmdsType;
	END_STRUCT;
END_TYPE

(*Semi auto mode*)

TYPE
	R3SemiAutoValueLimitsMainType : 	STRUCT 
		GCSLow : R3SemiAutoValueLimitsType;
		GCSHigh : R3SemiAutoValueLimitsType;
		ACSLow : R3SemiAutoValueLimitsType;
		ACSHigh : R3SemiAutoValueLimitsType;
	END_STRUCT;
	R3SemiAutoValueLimitsType : 	STRUCT 
		Val1 : LREAL;
		Val2 : LREAL;
		Val3 : LREAL;
		Val4 : LREAL;
		Val5 : LREAL;
		Val6 : LREAL;
	END_STRUCT;
	R3SemiAskedValueType : 	STRUCT  (*for SemiAuto*)
		Velocity : REAL := 50;
		TCS : ARRAY[0..5]OF REAL;
		ACS : ARRAY[0..5]OF REAL;
		GCS : ARRAY[0..5]OF REAL;
	END_STRUCT;
	R3SemiAutoEnumType : 
		(
		semiSTATE_INIT := 0,
		semiSTATE_START := 1,
		semiSTATE_UPDATE := 2,
		semiSTATE_GO := 3,
		semiSTATE_STOP := 5,
		semiSTATE_PAUSE := 4
		);
	R3SemiAutoModeType : 	STRUCT 
		AskedValue : R3SemiAskedValueType; (*Stores given axis distance for relative move*)
		Flag : BOOL; (*Flag between READY and SEMIAUTO*)
		Mode : BOOL; (*Switches between relative and absolute modes, 1 for Relative, 0 for Absolute*)
		UpdatePending : BOOL; (*if UpdatePending then updates before starting move*)
		State : R3SemiAutoEnumType; (*state selector for state machine*)
		ModeForThisMove : BOOL; (*makes sure that switching mode in time of moving doesnt bugs out move*)
		CoordinateSystem : McCoordinateSystemEnum; (*Stores selected coordinate system (0 - axis, 9 - global, 10 - tool) (no tool is set, so 9=10)*)
		PathMode : BOOL; (*Switches between direct and linear path mode, 1 for linear , 0 for direct*)
		PathModeForThisMove : BOOL;
		ExitSemiAuto : BOOL;
		Pause : BOOL;
		GroupRead : MC_BR_GroupReadCyclicPosition_15; (*Read current position in select coordinate system*)
		CurrentTCSPos : ARRAY[0..14]OF LREAL; (*Read positions*)
		MoveDone : BOOL;
		ValueLimits : R3SemiAutoValueLimitsMainType;
	END_STRUCT;
END_TYPE

(*Various usage*)

TYPE
	R3DirectionEnum : 
		( (*Currently unused*)
		POSITIVE := 1,
		NEGATIVE := -1
		);
	R3KeyCheckType : 	STRUCT 
		original_combo : ARRAY[0..4]OF USINT := [128,127,126,125,124];
		pending_combo : ARRAY[0..4]OF USINT;
		temp_value : USINT;
		value_changed : BOOL := FALSE;
		event_achieved : BOOL := FALSE;
	END_STRUCT;
	R3ManualModeType : 	STRUCT  (*Manual mode type*)
		AxisButton : R3AxisBtnType; (*Stores button states for select coordinate system*)
		CoordinateSystem : McCoordinateSystemEnum; (*Stores selected coordinate system (0 - axis, 9 - global, 10 - tool) (no tool is set, so 9=10)*)
		Direction : INT; (*Stores which direction should the axis be moved (1 - POSITIVE, -1 - NEGATIVE)*)
		JogVelocity : REAL := 20; (*Stores the velocity of axis*)
		PathLimits : McJogPathLimitsType; (*Stores set limits of acceleration and deceleration, velocity and jerk*)
		ActivateMove : BOOL; (*Enables or disables current move execution*)
		ExitManual : BOOL; (*Exits Manual Mode on True*)
		ToolOutput : BOOL; (*Tool output state*)
	END_STRUCT;
	R3RecipeType : 	STRUCT  (*Type used for recipe variable*)
		Acceleration : REAL;
		Deceleration : REAL;
		Jerk : REAL;
		Override : REAL;
		Velocity : REAL;
	END_STRUCT;
	R3StateMachineEnum : 
		(
		STATE_ERROR, (*Error state*)
		STATE_INIT, (*Initialisation state*)
		STATE_POWER_ON, (*Powering on state*)
		STATE_READY, (*Ready for commands*)
		STATE_MANUAL_CONTROL, (*Manual mode*)
		STATE_SEMI_AUTOMATIC, (*Semi automatic mode*)
		STATE_TEACHING, (*Teaching mode*)
		STATE_AUTOMATIC, (*Automatic mode (script execution)*)
		STATE_CALIBRATION, (*Calibration mode*)
		STATE_HOMING, (*Homing mode*)
		STATE_BRAKES, (*This will read brakes' status.*)
		STATE_DEMO_TILES
		);
	CommunicationType : 	STRUCT 
		Power : BOOL := FALSE;
		Pause : BOOL := FALSE;
		Stop : BOOL := FALSE;
		ErrorReset : BOOL := FALSE;
		changeModePending : USINT := 0; (*1-Manual       2-SemiManual      3-Auto 0-none*)
		CoordinateSystemManual : UDINT := 0; (*0 - ACS, 9 - TCS, 10 - GCS*)
		CoordinateSystemSemiAuto : UDINT := 0; (*0 - ACS, 9 - TCS, 10 - GCS*)
		SemiAutoCoordButtons : ARRAY[0..2]OF BOOL;
		txt_State_out : WSTRING[80];
		CalibBtn1To5 : BOOL := FALSE;
		CalibrationImage : STRING[80];
		TextManager : TextManagerType;
		RestoreAxis5 : BOOL := FALSE;
		CalibrateAgain : BOOL := FALSE;
		CalibrationStateBtn : BOOL; (*TRUE when in state STATE_CALIBRATION *)
		ResetHome : BOOL; (*Used in calibration*)
		txt_State_out_front : STRING[80];
		BrakeOpen : BOOL; (*open the brakes*)
		BrakeClose : BOOL; (*close the brakes*)
		BrakeReadStatus : BOOL; (*read status of brakes*)
		BrakesSet : BOOL; (*This variable tells the program if brake parameters have been set.*)
		ToolOutput : BOOL; (*This variable enables tool control - bound to M-function M6*)
		ToolOutputReset : BOOL; (*Mapped to M17 - used to reset M16*)
		ReturnToZero : BOOL; (*This variable allows returning to zero position in manual mode.*)
		WarmRestart : BOOL := FALSE;
		ReadWorkspace : BOOL; (*Not implemented.*)
		AlarmQueryResult : DINT;
		RecipeLoad : BOOL; (*if true then recipe is loaded into the robot*)
	END_STRUCT;
	ControlSelectEnum : 
		(
		ManualJog := 1,
		SemiAutomatic := 2,
		Automatic := 3,
		None := 0,
		Teaching := 4,
		DemoTiles := 5
		);
	R3WorkspaceType : 	STRUCT  (*this allows workspace to be set during runtime.*)
		Para : McCfgAxGrpFeatWsmType := (ModalDataBehaviour:=(Type:=mcAGFMDB_USE_AX_GRP_SET));
		Cmd : MC_BR_ProcessConfig;
		Done : BOOL; (*indicates the FB has finished*)
	END_STRUCT;
	TextManagerType : 	STRUCT 
		CalibHelper : STRING[80];
	END_STRUCT;
END_TYPE

(*Teaching mode*)

TYPE
	R3TeachingModeType : 	STRUCT  (*Main type for teaching mode, semi auto with extras*)
		Para : R3TeachingParaType;
		Info : R3TeachingInfoType;
		Cmds : R3TeachingCmdsType;
	END_STRUCT;
	R3TeachingStateEnum : 
		(
		teachSTATE_IDLE,
		teachSTATE_MOVING,
		teachSTATE_GENERATING,
		teachSTATE_SAVING
		);
	R3TeachingLastMoveType : 	STRUCT 
		CoordinateSystem : McCoordinateSystemEnum; (*Coordinate system of last move 0 - ACS, 10 - GCS*)
		PathMode : BOOL; (*1 - Linear, 0 - Direct*)
		MoveMode : BOOL; (*1 - Relative, 0 - Absolute*)
		Coordinates : ARRAY[0..5]OF REAL; (*Coordinates in select coordinate system*)
		Velocity : REAL; (*Velocity of movement*)
	END_STRUCT;
	R3TeachingInfoType : 	STRUCT 
		LastMove : R3TeachingLastMoveType; (*This variable saves the last successfully executed move*)
		CurrentState : R3TeachingStateEnum; (*Main state machine*)
		FileStatus : UINT; (*Error code from FileIO*)
		FileState : USINT; (*FileIO state machine*)
		FileInfo : fiFILE_INFO; (*Struct for FileInfo (used only to set offset in case a file exists)*)
		AllowSave : BOOL; (*Visualization variable (save button)*)
	END_STRUCT;
	R3TeachingParaType : 	STRUCT 
		SemiAutoModePara : R3SemiAutoModeType; (*semi auto mode parameters*)
		Save : BOOL; (*Triggers script saving*)
		FileName : STRING[80]; (*File name of generated script*)
		Command : STRING[160]; (*Generated command*)
		ToolBehaviour : BOOL; (*1 - Output after move, 0 - Output before move*)
		ToolOutput : BOOL; (*1 - tool output set*)
		ClearFile : BOOL;
	END_STRUCT;
	R3TeachingCmdsType : 	STRUCT 
		SemiAuto : R3SemiAutoMode; (*Semi auto mode FB*)
		FOpen : FileOpen;
		FWrite : FileWrite;
		FCreate : FileCreate;
		FInfo : FileInfo;
		FClose : FileClose;
	END_STRUCT;
END_TYPE
