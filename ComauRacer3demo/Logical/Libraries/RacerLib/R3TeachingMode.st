
FUNCTION_BLOCK R3TeachingMode
	
	IF Para.Para.SemiAutoModePara.ExitSemiAuto THEN
		Para.Cmds.SemiAuto(RoboArm := RoboArm, RoboArmPara := RoboArmPara, SemiAutoModePara := Para.Para.SemiAutoModePara);
		Para.Info.CurrentState := 1;
	END_IF
	
	CASE Para.Info.CurrentState OF
		teachSTATE_IDLE:
			
			IF Para.Para.FileName = '' OR NOT anyMoveDone THEN
				Para.Info.AllowSave := FALSE;
			END_IF
						
			Para.Info.FileState := 7;
			Para.Info.FileStatus := 0;
			
			IF Para.Para.Save THEN
				Para.Info.CurrentState := teachSTATE_SAVING;
			END_IF
			
			IF Para.Para.SemiAutoModePara.Flag THEN
				Para.Info.CurrentState := teachSTATE_MOVING;
				brsmemset(ADR(Para.Para.Command), 0, SIZEOF(Para.Para.Command));
			END_IF
			
		teachSTATE_MOVING:
			
			Para.Cmds.SemiAuto(RoboArm := RoboArm, RoboArmPara := RoboArmPara, SemiAutoModePara := Para.Para.SemiAutoModePara);
			
			IF Para.Para.SemiAutoModePara.MoveDone THEN
				anyMoveDone := TRUE;
				
				Para.Info.LastMove.MoveMode := Para.Para.SemiAutoModePara.ModeForThisMove; //1 for relative, 0 for absolute
				Para.Info.LastMove.PathMode := Para.Para.SemiAutoModePara.PathModeForThisMove; //1 for linear, 0 for direct
				Para.Info.LastMove.Velocity := RoboArmPara.Velocity;
				Para.Info.LastMove.CoordinateSystem := Para.Para.SemiAutoModePara.CoordinateSystem;
			
				IF Para.Info.LastMove.CoordinateSystem = 0 THEN
					Para.Info.LastMove.Coordinates[0] := Para.Para.SemiAutoModePara.AskedValue.ACS[0];
					Para.Info.LastMove.Coordinates[1] := Para.Para.SemiAutoModePara.AskedValue.ACS[1];
					Para.Info.LastMove.Coordinates[2] := Para.Para.SemiAutoModePara.AskedValue.ACS[2];
					Para.Info.LastMove.Coordinates[3] := Para.Para.SemiAutoModePara.AskedValue.ACS[3];
					Para.Info.LastMove.Coordinates[4] := Para.Para.SemiAutoModePara.AskedValue.ACS[4];
					Para.Info.LastMove.Coordinates[5] := Para.Para.SemiAutoModePara.AskedValue.ACS[5];
				
				ELSIF Para.Info.LastMove.CoordinateSystem = 9 THEN //unused for technical reasons
					Para.Info.LastMove.Coordinates[0] := Para.Para.SemiAutoModePara.AskedValue.TCS[0];
					Para.Info.LastMove.Coordinates[1] := Para.Para.SemiAutoModePara.AskedValue.TCS[1];
					Para.Info.LastMove.Coordinates[2] := Para.Para.SemiAutoModePara.AskedValue.TCS[2];
					Para.Info.LastMove.Coordinates[3] := Para.Para.SemiAutoModePara.AskedValue.TCS[3];
					Para.Info.LastMove.Coordinates[4] := Para.Para.SemiAutoModePara.AskedValue.TCS[4];
					Para.Info.LastMove.Coordinates[5] := Para.Para.SemiAutoModePara.AskedValue.TCS[5];
				
				ELSIF Para.Info.LastMove.CoordinateSystem = 10 THEN
					Para.Info.LastMove.Coordinates[0] := Para.Para.SemiAutoModePara.AskedValue.GCS[0];
					Para.Info.LastMove.Coordinates[1] := Para.Para.SemiAutoModePara.AskedValue.GCS[1];
					Para.Info.LastMove.Coordinates[2] := Para.Para.SemiAutoModePara.AskedValue.GCS[2];
					Para.Info.LastMove.Coordinates[3] := Para.Para.SemiAutoModePara.AskedValue.GCS[3];
					Para.Info.LastMove.Coordinates[4] := Para.Para.SemiAutoModePara.AskedValue.GCS[4];
					Para.Info.LastMove.Coordinates[5] := Para.Para.SemiAutoModePara.AskedValue.GCS[5];
				END_IF
				
				Para.Info.CurrentState := teachSTATE_GENERATING;
				Para.Info.AllowSave := TRUE;
				
			END_IF
			
		teachSTATE_GENERATING: //this state generates the string to be written to the file. Currently ignoring circular interpolation as this would require a GUI rework
			tempStrLen := 0;
			
			tempStr := '$nG108 ACC=';
			brsstrcat(ADR(Para.Para.Command), ADR(tempStr));
			brsftoa(RoboArmPara.Acceleration, ADR(tempStr));
			brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //acceleration parameter set
			
			tempStr := '$nG109 ACC=';
			brsstrcat(ADR(Para.Para.Command), ADR(tempStr));
			brsftoa(RoboArmPara.Deceleration, ADR(tempStr));
			brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //deceleration parameter set
			
			IF NOT Para.Para.ToolBehaviour AND Para.Para.ToolOutput THEN //set m-function before movement
				IF Para.Para.ToolOutput THEN
					tempStr := '$nM16';
				ELSE
					tempStr := '$nM17'; //reset tool output
				END_IF
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr));
			END_IF
			
			IF NOT Para.Info.LastMove.MoveMode THEN
				tempStr := '$nG90$n'; //absolute
			ELSE
				tempStr := '$nG91$n'; //relative
			END_IF
			
			brsstrcat(ADR(Para.Para.Command), ADR(tempStr));
			
			IF NOT Para.Info.LastMove.PathMode THEN
				tempStr := 'G101'; //direct
			ELSE
				tempStr := 'G01'; //linear
			END_IF
			
			brsstrcat(ADR(Para.Para.Command), ADR(tempStr));
			
			IF Para.Info.LastMove.CoordinateSystem = 0 THEN
				
				tempStr := ' Q1=';
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //Q1
				brsftoa(Para.Info.LastMove.Coordinates[0],ADR(tempStr));
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //actual value
				
				tempStr := ' Q2=';
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //Q2
				brsftoa(Para.Info.LastMove.Coordinates[1],ADR(tempStr));
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //actual value
				
				tempStr := ' Q3=';
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //Q3
				brsftoa(Para.Info.LastMove.Coordinates[2],ADR(tempStr));
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //actual value
				
				tempStr := ' Q4=';
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //Q4
				brsftoa(Para.Info.LastMove.Coordinates[3],ADR(tempStr));
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //actual value
				
				tempStr := ' Q5=';
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //Q5
				brsftoa(Para.Info.LastMove.Coordinates[4],ADR(tempStr));
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //actual value
				
				tempStr := ' Q6=';
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //Q6
				brsftoa(Para.Info.LastMove.Coordinates[5],ADR(tempStr));
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //actual value
				
			ELSIF Para.Info.LastMove.CoordinateSystem = 10 THEN
				tempStr := ' X';
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //Q1
				brsftoa(Para.Info.LastMove.Coordinates[0],ADR(tempStr));
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //actual value
				
				tempStr := ' Y';
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //Q2
				brsftoa(Para.Info.LastMove.Coordinates[1],ADR(tempStr));
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //actual value
				
				tempStr := ' Z';
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //Q3
				brsftoa(Para.Info.LastMove.Coordinates[2],ADR(tempStr));
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //actual value
				
				tempStr := ' A';
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //Q4
				brsftoa(Para.Info.LastMove.Coordinates[3],ADR(tempStr));
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //actual value
				
				tempStr := ' B';
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //Q5
				brsftoa(Para.Info.LastMove.Coordinates[4],ADR(tempStr));
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //actual value
				
				tempStr := ' C';
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //Q6
				brsftoa(Para.Info.LastMove.Coordinates[5],ADR(tempStr));
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr)); //actual value
			END_IF //no support for TCS in G-Code (as far as I can tell)
			
			
			tempStr := ' F';
			brsstrcat(ADR(Para.Para.Command), ADR(tempStr));
			brsftoa(Para.Info.LastMove.Velocity * 60, ADR(tempStr)); //60 is the conversion rate from mm/s to F-stops, so F-stops are based on minutes? F1000 = 1000 mm per minute?
			brsstrcat(ADR(Para.Para.Command), ADR(tempStr));
			
			IF Para.Para.ToolBehaviour AND Para.Para.ToolOutput THEN //set m-function after movement
				IF Para.Para.ToolOutput THEN
					tempStr := '$nM16';
				ELSE
					tempStr := '$nM17';
				END_IF
				brsstrcat(ADR(Para.Para.Command), ADR(tempStr));
			END_IF
			
			tempStrLen := brsstrlen(ADR(Para.Para.Command));
			Para.Info.CurrentState := teachSTATE_IDLE;
			
		teachSTATE_SAVING:
			CASE Para.Info.FileState OF
				0: (**** Error step ****)
					Para.Para.Save := FALSE;
					Para.Info.CurrentState := teachSTATE_IDLE;
					
				1: (**** Try to open existing file ****)
					(* Initialize file open structrue *)
					Para.Cmds.FOpen.enable    := 1;
					Para.Cmds.FOpen.pDevice   := ADR('CNC_PrgDir');
					Para.Cmds.FOpen.pFile     := ADR(tempStr);
					Para.Cmds.FOpen.mode      := fiREAD_WRITE;  (* Read and write access *)
					(* Call FBK *)
					Para.Cmds.FOpen();
					(* Get FBK output information *)
					dwIdent := Para.Cmds.FOpen.ident;
					(* Verify status (20708 -> File doesn't exist) *)
					IF (Para.Cmds.FOpen.status = 20708) THEN
						Para.Info.FileState := 2;
					ELSE
						IF (Para.Cmds.FOpen.status = 0) THEN
							Para.Info.FileState := 3;
						ELSE
							IF (Para.Cmds.FOpen.status <> 65535) THEN
								Para.Info.FileStatus := Para.Cmds.FInfo.status;
								Para.Info.FileState := 0;
							END_IF
						END_IF
					END_IF
					
				2: (**** Create file ****)
					(* Initialize file create structure *)
					Para.Cmds.FCreate.enable  := 1;
					Para.Cmds.FCreate.pDevice := ADR('CNC_PrgDir');
					Para.Cmds.FCreate.pFile   := ADR(tempStr);
					(* Call FBK *)
					Para.Cmds.FCreate();
					(* Get FBK output information *)
					dwIdent := Para.Cmds.FCreate.ident;
					(* Verify status *)
					IF (Para.Cmds.FCreate.status = 0) THEN
						Para.Info.FileState := 3;
					ELSE
						IF (Para.Cmds.FCreate.status <> 65535) THEN
							Para.Info.FileStatus := Para.Cmds.FInfo.status;
							Para.Info.FileState := 0;
						END_IF
					END_IF
					
				3:
					Para.Cmds.FInfo.enable    := 1;
					Para.Cmds.FInfo.pDevice   := ADR('CNC_PrgDir');
					Para.Cmds.FInfo.pName     := ADR(tempStr);
					Para.Cmds.FInfo.pInfo     := ADR(Para.Info.FileInfo);
					(* Call FBK *)
					Para.Cmds.FInfo();
					(* Get FBK output information *)
					(* Verify status (20708 -> File doesn't exist) *)
					IF (Para.Cmds.FInfo.status = 20708) THEN
						Para.Info.FileState := 2;
					ELSE
						IF (Para.Cmds.FInfo.status = 0) THEN
							Para.Info.FileState := 4;
						ELSE
							IF (Para.Cmds.FInfo.status <> 65535) THEN
								Para.Info.FileStatus := Para.Cmds.FInfo.status;
								Para.Info.FileState := 0;
							END_IF
						END_IF
					END_IF
					
				4: (**** Write data to file ****)
					(* Initialize file write structure *)
					Para.Cmds.FWrite.enable   := 1;
					Para.Cmds.FWrite.ident    := dwIdent;
					Para.Cmds.FWrite.offset   := Para.Info.FileInfo.size; //append text
					Para.Cmds.FWrite.pSrc     := ADR(Para.Para.Command);
					Para.Cmds.FWrite.len      := tempStrLen;
					(* Call FBK *)
					Para.Cmds.FWrite();
					(* Get status *)
					(* Verify status *)
					IF (Para.Cmds.FWrite.status = 0) THEN
						Para.Info.FileState := 5;
					ELSE
						IF (Para.Cmds.FWrite.status <> 65535) THEN
							Para.Info.FileStatus := Para.Cmds.FInfo.status;
							Para.Info.FileState := 0;
	
						END_IF
					END_IF
				5: (* close file *)
					Para.Cmds.FClose.enable := 1;
					Para.Cmds.FClose.ident := dwIdent;
					Para.Cmds.FClose();
					IF (Para.Cmds.FClose.status = 0) THEN
						Para.Info.FileState := 6;
					ELSE
						IF (Para.Cmds.FClose.status <> 65535) THEN
							Para.Info.FileStatus := Para.Cmds.FInfo.status;
							Para.Info.FileState := 0;
	
						END_IF
					END_IF
				
				6: (* go to initial state *)
					Para.Para.Save := FALSE;
					Para.Info.AllowSave := FALSE; //move has been saved, don't allow repetition
					Para.Info.CurrentState := teachSTATE_IDLE;
				
				7: (* shrink filename string *)
					brsmemset(ADR(tempStr), 0, SIZEOF(tempStr)); // nullify tempStr
					brsmemmove(ADR(tempStr), ADR(Para.Para.FileName)+brsstrlen(ADR('CNC_PrgDir/')), brsstrlen(ADR(Para.Para.FileName))-brsstrlen(ADR('CNC_PrgDir/'))); //copy filename without file device / to tempStr
					Para.Info.FileState := 1;
					
			END_CASE
		
	END_CASE


END_FUNCTION_BLOCK	
