
(* TODO: Add your comment here
ActivateMove and AxisSaveBtn are assumed momentary. 
not resetting AxisSaveBtn and switching AxisButton will result in immediate AxisCalibrated[i] := TRUE!
not resetting ActivateMove might result in damage to physical system! *)
FUNCTION_BLOCK R3CalibrationMode
		
	IF CalibrationPara.CalibrationModePara.Direction = 1 THEN // set actual axis velocity based on which direction it's set to be moved
		
		JogVelocityActual := CalibrationPara.CalibrationModePara.JogVelocity;
		
	ELSIF CalibrationPara.CalibrationModePara.Direction = -1 THEN
		
		JogVelocityActual := -1 * CalibrationPara.CalibrationModePara.JogVelocity;
		
	END_IF
	
	IF NOT CalibrationPara.Axis5To6Para.MoveAxis AND NOT CalibrationPara.Axis5To6Para.HomeReturn THEN
		RoboArm.MoveDirect := FALSE;
	END_IF
	
	IF RoboArmPara.Velocity <> CalibrationPara.Axis5To6Para.AxisMoveVelocity THEN
		RoboArmPara.Velocity := CalibrationPara.Axis5To6Para.AxisMoveVelocity; //please use a low velocity for this (just to be safe)
		
	END_IF
		
	CASE CalibrationPara.CalibrationState OF
		STATE_START:
			CalibrationPara.AxisSaveBtn := FALSE; //reset button status in case someone clicks when axis isn't ready
			CalibrationPara.SaveEnable := FALSE;
			CalibrationPara.Axis5To6Para.MoveAxis := FALSE; //this stops movement of axis to predefined position
			CalibrationPara.Axis5To6Para.MoveEnable := FALSE;
			//CalibrationPara.Axis5To6Para.HomeReturn := FALSE;
			CalibrationPara.Axis5To6Para.Axis2Moved := FALSE; //reset these flags to make calibration less buggy and complicated
			CalibrationPara.Axis5To6Para.Axis3Moved := FALSE;
			CalibrationPara.Axis5To6Para.Axis5Moved := FALSE;
			
			IF RoboArm.MoveDirect THEN
				RoboArm.MoveDirect := FALSE; //this stops movement of axis to predefined position
			END_IF
						
			RoboArmPara.Jog.CoordSystem := CalibrationPara.CalibrationModePara.CoordinateSystem;
			RoboArmPara.Jog.PathLimits := CalibrationPara.CalibrationModePara.PathLimits;
			
			IF NOT CalibrationPara.CalibrationModePara.ActivateMove THEN //disable jog if "Move" flag isn't set
				RoboArm.Jog := FALSE;
				FOR j := 0 TO 5 BY 1 DO //set every jog velocity to 0
					RoboArmPara.Jog.Velocity[j] := 0.0;
				END_FOR;
			END_IF
			
			FOR j := 0 TO 5 BY 1 DO //set every distance to 0
				RoboArmPara.Position[j] := 0.0;
			END_FOR;
			
			IF CalibrationPara.CalibrationModePara.AxisButton.Q1 THEN // if a button is pressed, write corresponding velocity to the jog velocity array
				CalibrationPara.CalibrationState := STATE_CALIBRATE_Q1;
			
			ELSIF CalibrationPara.CalibrationModePara.AxisButton.Q2 THEN
				CalibrationPara.CalibrationState := STATE_CALIBRATE_Q2;
			
			ELSIF CalibrationPara.CalibrationModePara.AxisButton.Q3 THEN
				CalibrationPara.CalibrationState := STATE_CALIBRATE_Q3;
			
			ELSIF CalibrationPara.CalibrationModePara.AxisButton.Q4 THEN
				CalibrationPara.CalibrationState := STATE_CALIBRATE_Q4;
			
			ELSIF CalibrationPara.CalibrationModePara.AxisButton.Q5 THEN //don't permit axis 5 calibration until all the previous are calibrated!
				CalibrationPara.CalibrationState := STATE_CALIBRATE_Q5;
			
			ELSIF CalibrationPara.CalibrationModePara.AxisButton.Q6 THEN //don't permit axis 6 calibration until all the others are calibrated!
				CalibrationPara.CalibrationState := STATE_CALIBRATE_Q6;
			END_IF
			
			IF CalibrationPara.Axis5To6Para.HomeReturn AND CalibrationPara.AxisCalibrated[5] AND NOT CalibrationPara.Axis5To6Para.AxesRestored THEN 
				//only allow this if axis 6 has been calibrated (Show button in HMI)
				CalibrationPara.CalibrationState := STATE_RESTORE_HOME;
			END_IF			
			
		STATE_CALIBRATE_Q1:
			RoboArmPara.Jog.Velocity[0] := JogVelocityActual; //write jog velocity to robot
			CalibrationPara.SaveEnable := TRUE; //this enables the "Save axis" button in HMI.
			
			IF CalibrationPara.CalibrationModePara.ActivateMove THEN //enable jog only if ActivateMove flag is set
				RoboArm.Jog := TRUE;
			ELSE
				RoboArm.Jog := FALSE;
			END_IF
			
			IF CalibrationPara.AxisSaveBtn THEN //set Calibrated flag for given axis if AxisSaveBtn flag is set
				RoboArm.Jog := FALSE;
				FOR j := 0 TO 5 BY 1 DO
					IF j = 0 THEN
						CalibrationPara.Positions[j] := 0.0;
					ELSE
						CalibrationPara.Positions[j] := RoboArm.Info.JointAxisPosition[j];
					END_IF
					
				END_FOR;
				
				CalibrationPara.GroupHome(AxesGroup := CalibrationPara.AxesGroupRef, Execute := TRUE, Position := CalibrationPara.Positions, HomingMode := CalibrationPara.FakeHomingModeEnum); //this makes the robot think its axes are 0.0
				IF CalibrationPara.GroupHome.Done THEN
					CalibrationPara.GroupHome(Execute := FALSE);
					CalibrationPara.AxisCalibrated[0] := TRUE;
					CalibrationPara.AxisSaveBtn := FALSE;
				END_IF
				
			ELSIF NOT CalibrationPara.CalibrationModePara.AxisButton.Q1 THEN //go back if button is released
				
				CalibrationPara.CalibrationState := STATE_START;
			END_IF
			
			
		STATE_CALIBRATE_Q2:
			RoboArmPara.Jog.Velocity[1] := JogVelocityActual;
			CalibrationPara.SaveEnable := TRUE; //this enables the "Save axis" button in HMI.
			
			IF CalibrationPara.CalibrationModePara.ActivateMove THEN //enable jog only if ActivateMove flag is set
				RoboArm.Jog := TRUE;
			ELSE
				RoboArm.Jog := FALSE;
			END_IF
			
			IF CalibrationPara.AxisSaveBtn THEN //set Calibrated flag for given axis if AxisSaveBtn flag is set
				RoboArm.Jog := FALSE;
				FOR j := 0 TO 5 BY 1 DO
					IF j = 1 THEN
						CalibrationPara.Positions[j] := 0.0;
					ELSE
						CalibrationPara.Positions[j] := RoboArm.Info.JointAxisPosition[j];
					END_IF
					
				END_FOR;
				
				CalibrationPara.GroupHome(AxesGroup := CalibrationPara.AxesGroupRef, Execute := TRUE, Position := CalibrationPara.Positions, HomingMode := CalibrationPara.FakeHomingModeEnum); //this makes the robot think its axes are 0.0
				IF CalibrationPara.GroupHome.Done THEN
					CalibrationPara.GroupHome(Execute := FALSE);
					CalibrationPara.AxisCalibrated[1] := TRUE;
					CalibrationPara.AxisSaveBtn := FALSE;
				END_IF
				
			ELSIF NOT CalibrationPara.CalibrationModePara.AxisButton.Q2 THEN
				CalibrationPara.CalibrationState := STATE_START;
			END_IF
			
		STATE_CALIBRATE_Q3:
			(*RoboArmPara.Jog.Velocity[2] := JogVelocityActual;
			CalibrationPara.SaveEnable := TRUE; //this enables the "Save axis" button in HMI.
			
			IF CalibrationPara.CalibrationModePara.ActivateMove THEN //enable jog only if ActivateMove flag is set
				RoboArm.Jog := TRUE;
			ELSE
				RoboArm.Jog := FALSE;
			END_IF
			
			IF CalibrationPara.AxisSaveBtn THEN //set Calibrated flag for given axis if AxisSaveBtn flag is set
				CalibrationPara.AxisCalibrated[2] := TRUE;
				CalibrationPara.InitHomePara.Position := -90.0;
				CalibrationPara.InitHome(Axis := (CalibrationPara.AxesRef[2]), Execute := TRUE, HomingParameters := CalibrationPara.InitHomePara);
				CalibrationPara.SavedPositions[2] := CalibrationPara.InitHomePara.Position;
				RoboArm.Jog := FALSE;
				CalibrationPara.AxisSaveBtn := FALSE;
			END_IF
			
			IF NOT CalibrationPara.CalibrationModePara.AxisButton.Q3 THEN
				CalibrationPara.CalibrationState := STATE_START;
			END_IF*) //old code without moving of axis 2
			
			IF CalibrationPara.AxisCalibrated[0] AND CalibrationPara.AxisCalibrated[1] THEN //if axes 1 and 2 have been calibrated
				(*one-time execution of absolute movement of axis 2 to +25 degrees*)
				IF NOT CalibrationPara.Axis5To6Para.Axis2Moved AND NOT RoboArm.MoveActive AND NOT RoboArm.MoveDirect AND CalibrationPara.Axis5To6Para.MoveAxis THEN
					
					RoboArmPara.Position[0] := CalibrationPara.Axis5To6Para.AxisAngles[0]; //axis 1
					RoboArmPara.Position[1] := CalibrationPara.Axis5To6Para.AxisAngles[1]; //axis 2 may be moved this amount in order to make calibrating axis 5 and 6 easier (default +25, set in Types.typ)
					//don't move other axes
					RoboArmPara.Position[2] := RoboArm.Info.JointAxisPosition[2]; //axis 3
					RoboArmPara.Position[3] := RoboArm.Info.JointAxisPosition[3]; //axis 4
					RoboArmPara.Position[4] := RoboArm.Info.JointAxisPosition[4]; //axis 5
					RoboArmPara.Position[5] := RoboArm.Info.JointAxisPosition[5]; //axis 6 
					
					RoboArmPara.ManualMoveType := mcMOVE_ABSOLUTE; //select absolute movement (to restore position after calibrating axis 6)
					RoboArmPara.CoordSystem := 0; // select ACS for moving
					RoboArm.MoveDirect := TRUE; //execute movement
					
				ELSIF RoboArm.MoveDone AND RoboArm.MoveDirect THEN //if move has been executed
					
					RoboArm.MoveDirect := FALSE; //reset move flag
					CalibrationPara.Axis5To6Para.Axis2Moved := TRUE; //set flag to let the program know axis 5 has been moved to correct position
					CalibrationPara.Axis5To6Para.MoveAxis := FALSE;
					CalibrationPara.Axis5To6Para.MoveEnable := FALSE;
					
				ELSIF CalibrationPara.Axis5To6Para.Axis2Moved THEN //actual calibration
					CalibrationPara.SaveEnable := TRUE; //this enables the "Save axis" button in HMI.
					
					RoboArmPara.Jog.Velocity[2] := JogVelocityActual;
			
					IF CalibrationPara.CalibrationModePara.ActivateMove THEN //enable jog only if ActivateMove flag is set
						RoboArm.Jog := TRUE;
					ELSE
						RoboArm.Jog := FALSE;
					END_IF
			
					IF CalibrationPara.AxisSaveBtn THEN //set Calibrated flag for given axis if AxisSaveBtn flag is set
						RoboArm.Jog := FALSE;
						FOR j := 0 TO 5 BY 1 DO
							IF j = 2 THEN
								CalibrationPara.Positions[j] := -90.0;
							ELSE
								CalibrationPara.Positions[j] := RoboArm.Info.JointAxisPosition[j];
							END_IF
					
						END_FOR;
				
						CalibrationPara.GroupHome(AxesGroup := CalibrationPara.AxesGroupRef, Execute := TRUE, Position := CalibrationPara.Positions, HomingMode := CalibrationPara.FakeHomingModeEnum); //this makes the robot think its axes are 0.0
						IF CalibrationPara.GroupHome.Done THEN
							CalibrationPara.GroupHome(Execute := FALSE);
							CalibrationPara.AxisCalibrated[2] := TRUE;
							CalibrationPara.AxisSaveBtn := FALSE;
						END_IF
				
					END_IF
					
				ELSE
					//CalibrationPara.Axis5To6Para.MoveAxis := FALSE;
					CalibrationPara.Axis5To6Para.MoveEnable := TRUE;
				END_IF
				
			END_IF
			
			IF NOT CalibrationPara.CalibrationModePara.AxisButton.Q3 THEN
				CalibrationPara.CalibrationState := STATE_START;
			END_IF
			
		STATE_CALIBRATE_Q4:
			RoboArmPara.Jog.Velocity[3] := JogVelocityActual;
			CalibrationPara.SaveEnable := TRUE; //this enables the "Save axis" button in HMI.
			
			IF CalibrationPara.CalibrationModePara.ActivateMove THEN //enable jog only if ActivateMove flag is set
				RoboArm.Jog := TRUE;
			ELSE
				RoboArm.Jog := FALSE;
			END_IF
			
			IF CalibrationPara.AxisSaveBtn THEN //set Calibrated flag for given axis if AxisSaveBtn flag is set
				RoboArm.Jog := FALSE;
				FOR j := 0 TO 5 BY 1 DO
					IF j = 3 THEN
						CalibrationPara.Positions[j] := 0.0;
					ELSE
						CalibrationPara.Positions[j] := RoboArm.Info.JointAxisPosition[j];
					END_IF
					
				END_FOR;
				
				CalibrationPara.GroupHome(AxesGroup := CalibrationPara.AxesGroupRef, Execute := TRUE, Position := CalibrationPara.Positions, HomingMode := CalibrationPara.FakeHomingModeEnum); //this makes the robot think its axes are 0.0
				IF CalibrationPara.GroupHome.Done THEN
					CalibrationPara.GroupHome(Execute := FALSE);
					CalibrationPara.AxisCalibrated[3] := TRUE;
					CalibrationPara.AxisSaveBtn := FALSE;
				END_IF
				
			ELSIF NOT CalibrationPara.CalibrationModePara.AxisButton.Q4 THEN
				CalibrationPara.CalibrationState := STATE_START;
			END_IF
			
		STATE_CALIBRATE_Q5:
			
			IF CalibrationPara.AxisCalibrated[0] AND CalibrationPara.AxisCalibrated[1] AND CalibrationPara.AxisCalibrated[2] AND CalibrationPara.AxisCalibrated[3] THEN //if axes 1-4 have been calibrated
				(*one-time execution of absolute movement of axis 3 by -45 degrees*)
				IF NOT CalibrationPara.Axis5To6Para.Axis3Moved AND NOT RoboArm.MoveActive AND NOT RoboArm.MoveDirect AND CalibrationPara.Axis5To6Para.MoveAxis THEN
					
					RoboArmPara.Position[0] := CalibrationPara.Axis5To6Para.AxisAngles[0]; //axis 1 (default 0.0)
					RoboArmPara.Position[1] := CalibrationPara.Axis5To6Para.AxisAngles[1]; //axis 2 may be moved this amount in order to make calibrating axis 5 and 6 easier (default +25, set in Types.typ)
					RoboArmPara.Position[2] := CalibrationPara.Axis5To6Para.AxisAngles[2]; //axis 3 (default 0.0)
					RoboArmPara.Position[3] := CalibrationPara.Axis5To6Para.AxisAngles[3]; //axis 4 (default -135)
					//don't move other axes
					RoboArmPara.Position[4] := RoboArm.Info.JointAxisPosition[4]; //axis 5
					RoboArmPara.Position[5] := RoboArm.Info.JointAxisPosition[5]; //axis 6 
					RoboArmPara.ManualMoveType := mcMOVE_ABSOLUTE; //select absolute movement (to restore position after calibrating axis 6)
					RoboArmPara.CoordSystem := 0; // select ACS for moving
					RoboArm.MoveDirect := TRUE; //execute movement
					
				ELSIF RoboArm.MoveDone AND RoboArm.MoveDirect THEN //if move has been executed
					
					RoboArm.MoveDirect := FALSE; //reset move flag
					CalibrationPara.Axis5To6Para.Axis2Moved := TRUE;
					CalibrationPara.Axis5To6Para.Axis3Moved := TRUE; //set flag to let the program know axis 5 has been moved to correct position
					CalibrationPara.Axis5To6Para.MoveAxis := FALSE;
					CalibrationPara.Axis5To6Para.MoveEnable := FALSE;
					
				ELSIF CalibrationPara.Axis5To6Para.Axis3Moved THEN //actual calibration
					CalibrationPara.SaveEnable := TRUE; //this enables the "Save axis" button in HMI.
					
					RoboArmPara.Jog.Velocity[4] := JogVelocityActual;
			
					IF CalibrationPara.CalibrationModePara.ActivateMove THEN //enable jog only if ActivateMove flag is set
						RoboArm.Jog := TRUE;
					ELSE
						RoboArm.Jog := FALSE;
					END_IF
			
					IF CalibrationPara.AxisSaveBtn THEN //set Calibrated flag for given axis if AxisSaveBtn flag is set
						RoboArm.Jog := FALSE;
						FOR j := 0 TO 5 BY 1 DO
							IF j = 4 THEN
								CalibrationPara.Positions[j] := 0.0;
							ELSE
								CalibrationPara.Positions[j] := RoboArm.Info.JointAxisPosition[j];
							END_IF
					
						END_FOR;
				
						CalibrationPara.GroupHome(AxesGroup := CalibrationPara.AxesGroupRef, Execute := TRUE, Position := CalibrationPara.Positions, HomingMode := CalibrationPara.FakeHomingModeEnum); //this makes the robot think its axes are 0.0
						IF CalibrationPara.GroupHome.Done THEN
							CalibrationPara.GroupHome(Execute := FALSE);
							CalibrationPara.AxisCalibrated[4] := TRUE;
							CalibrationPara.AxisSaveBtn := FALSE;
						END_IF
				
					END_IF
				
				ELSE
					//CalibrationPara.Axis5To6Para.MoveAxis := FALSE;
					CalibrationPara.Axis5To6Para.MoveEnable := TRUE;
				END_IF
				
			END_IF
			
			IF NOT CalibrationPara.CalibrationModePara.AxisButton.Q5 THEN
				CalibrationPara.CalibrationState := STATE_START;
			END_IF
			
		STATE_CALIBRATE_Q6:
			(* if all previous axes have been calibrated and set in place *)
			IF CalibrationPara.AxisCalibrated[0] AND CalibrationPara.AxisCalibrated[1] AND CalibrationPara.AxisCalibrated[2] AND CalibrationPara.AxisCalibrated[3] AND CalibrationPara.AxisCalibrated[4] THEN
				(*one-time execution of absolute movement of axis 5 by -118 degrees*)
				IF NOT CalibrationPara.Axis5To6Para.Axis5Moved AND NOT RoboArm.MoveActive AND NOT RoboArm.MoveDirect AND CalibrationPara.Axis5To6Para.MoveAxis THEN
					
					RoboArmPara.Position[0] := CalibrationPara.Axis5To6Para.AxisAngles[0]; //axis 1 (default 0.0)
					RoboArmPara.Position[1] := CalibrationPara.Axis5To6Para.AxisAngles[1]; //axis 2 may be moved this amount in order to make calibrating axis 5 and 6 easier (default +25, set in Types.typ)
					RoboArmPara.Position[2] := CalibrationPara.Axis5To6Para.AxisAngles[2]; //axis 3 (default 0.0)
					RoboArmPara.Position[3] := CalibrationPara.Axis5To6Para.AxisAngles[3]; //axis 4 (default -135)
					RoboArmPara.Position[4] := CalibrationPara.Axis5To6Para.AxisAngles[4]; //axis 5 (default and REQUIRED -118 degrees)
					//don't move axis 6
					RoboArmPara.Position[5] := RoboArm.Info.JointAxisPosition[5]; //axis 6
					RoboArmPara.ManualMoveType := mcMOVE_ABSOLUTE; //select absolute movement (to restore position after calibrating axis 6)
					RoboArmPara.CoordSystem := 0; // select ACS for moving
					RoboArm.MoveDirect := TRUE; //execute movement
					
				ELSIF RoboArm.MoveDone AND RoboArm.MoveDirect THEN //if move has been executed
					
					RoboArm.MoveDirect := FALSE; //reset move flag
					CalibrationPara.Axis5To6Para.Axis2Moved := TRUE;
					CalibrationPara.Axis5To6Para.Axis3Moved := TRUE;
					CalibrationPara.Axis5To6Para.Axis5Moved := TRUE; //set flag to let the program know axis 5 has been moved to correct position
					CalibrationPara.Axis5To6Para.MoveAxis := FALSE;
					CalibrationPara.Axis5To6Para.MoveEnable := FALSE;
					
				ELSIF CalibrationPara.Axis5To6Para.Axis5Moved THEN //actual calibration
					CalibrationPara.SaveEnable := TRUE; //this enables the "Save axis" button in HMI.
					
					RoboArmPara.Jog.Velocity[5] := JogVelocityActual;
				
					IF CalibrationPara.CalibrationModePara.ActivateMove THEN //enable jog only if ActivateMove flag is set (momentary switch in HMI)
						RoboArm.Jog := TRUE;
					ELSE
						RoboArm.Jog := FALSE;
					END_IF
				
					IF CalibrationPara.AxisSaveBtn THEN //set Calibrated flag for given axis if AxisSaveBtn flag is set
						RoboArm.Jog := FALSE;
						FOR j := 0 TO 5 BY 1 DO
							IF j = 5 THEN
								CalibrationPara.Positions[j] := 0.0;
							ELSE
								CalibrationPara.Positions[j] := RoboArm.Info.JointAxisPosition[j];
							END_IF
					
						END_FOR;
				
						CalibrationPara.GroupHome(AxesGroup := CalibrationPara.AxesGroupRef, Execute := TRUE, Position := CalibrationPara.Positions, HomingMode := CalibrationPara.FakeHomingModeEnum); //this makes the robot think its axes are 0.0
						IF CalibrationPara.GroupHome.Done THEN
							CalibrationPara.GroupHome(Execute := FALSE);
							CalibrationPara.AxisCalibrated[5] := TRUE;
							CalibrationPara.AxisSaveBtn := FALSE;
						END_IF
				
					END_IF
				
				ELSE
					//CalibrationPara.Axis5To6Para.MoveAxis := FALSE;
					CalibrationPara.Axis5To6Para.MoveEnable := TRUE;
				END_IF
				
			END_IF
			
			IF NOT CalibrationPara.CalibrationModePara.AxisButton.Q6 THEN
				CalibrationPara.CalibrationState := STATE_START;
			END_IF
		
		STATE_RESTORE_HOME: //this returns the robot to 0,0,-90,0,0,0 position in ACS
			
			IF NOT RoboArm.MoveActive AND NOT RoboArm.MoveDirect THEN
					
				FOR j := 0 TO 5 BY 1 DO //set every position to 0 (axis 3's default is -90)
					IF j = 2 THEN
						RoboArmPara.Position[j] := -90.0;
					ELSE
						
						RoboArmPara.Position[j] := 0.0;
					END_IF
					
				END_FOR;

				RoboArmPara.ManualMoveType := mcMOVE_ABSOLUTE; //select relative movement (to restore position after calibrating axis 6)
				RoboArmPara.CoordSystem := 0; // select ACS for moving
				RoboArm.MoveDirect := TRUE; //execute movement
					
			ELSIF RoboArm.MoveDone AND RoboArm.MoveDirect THEN //if move has been executed
					
				RoboArm.MoveDirect := FALSE; //reset move flag
				CalibrationPara.Axis5To6Para.Axis2Moved := FALSE;
				CalibrationPara.Axis5To6Para.Axis3Moved := FALSE;
				CalibrationPara.Axis5To6Para.Axis5Moved := FALSE; //set flag to let the program know axis 5 has been moved to correct position
				CalibrationPara.Axis5To6Para.AxesRestored := TRUE;
				
			END_IF
			
			IF CalibrationPara.Axis5To6Para.AxesRestored OR NOT CalibrationPara.Axis5To6Para.HomeReturn THEN
				CalibrationPara.Axis5To6Para.HomeReturn := FALSE;
				CalibrationPara.CalibrationState := STATE_START;
			END_IF
		
	END_CASE
	
END_FUNCTION_BLOCK
