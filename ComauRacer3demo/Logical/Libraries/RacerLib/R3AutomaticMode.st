
(* TODO: Add your comment here *)
FUNCTION_BLOCK R3AutomaticMode
	
	Parameters.NOTExecute := NOT Parameters.Execute;
	
	IF Parameters.Abort THEN
		Info.CurrentState := autoSTATE_ABORT;
	END_IF
	
	IF Info.LastProgram = Parameters.ProgramName AND Info.LastProgram <> '' THEN
		//RoboArmPara.MoveProgramOptions.StartMode := mcPRGSM_NON_MODAL;
		//Info.FileGenerated := TRUE;
		IF Info.FileCopied THEN
			NowCopy := FALSE;
			IF Info.Show3D THEN
					Info.PreviewFilePath := '/FileDevice:CNC_PrgDir/PathPreview3DLast.svg';
					Info.PreviewOverlay := '';
				ELSE
					Info.PreviewFilePath := '/FileDevice:CNC_PrgDir/PathPreviewLast.svg';
					Info.PreviewOverlay := '<foreignObject x="0" y="0" width="470" height="470"><img src="Media/Coordsys.png" width="470" height="470"/></foreignObject>';
				END_IF
			
		ELSIF NowCopy THEN
			
			IF Info.PreviewFileStatus = 0 AND NOT Info.FileCopied AND NOT TempFlag THEN
				Parameters.FileCopy.enable := TRUE;
				Parameters.FileCopy.option := fiOVERWRITE;
				Parameters.FileCopy.pDestDev := ADR('CNC_PrgDir');
				Parameters.FileCopy.pSrcDev := ADR('CNC_PrgDir');
				Parameters.FileCopy.pSrc := ADR('PathPreview3D.svg');
				Parameters.FileCopy.pDest := ADR('PathPreview3DLast.svg');
				Parameters.FileCopy();
				IF Parameters.FileCopy.status = 0 THEN
					Parameters.FileCopy.enable := FALSE;
					TempFlag := TRUE;
					Parameters.FileCopy();
				END_IF
				
				
			ELSIF Info.Preview3DFileStatus = 0 AND NOT Info.FileCopied AND TempFlag THEN
				Parameters.FileCopy.enable := TRUE;
				Parameters.FileCopy.option := fiOVERWRITE;
				Parameters.FileCopy.pDestDev := ADR('CNC_PrgDir');
				Parameters.FileCopy.pSrcDev := ADR('CNC_PrgDir');
				Parameters.FileCopy.pSrc := ADR('PathPreview.svg');
				Parameters.FileCopy.pDest := ADR('PathPreviewLast.svg');
				Parameters.FileCopy();
				IF Parameters.FileCopy.status = 0 THEN
					Parameters.FileCopy.enable := FALSE;
					TempFlag := FALSE;
					Info.FileCopied := TRUE;
					Parameters.FileCopy();
				END_IF
				
			
			END_IF
		ELSE
			Info.PreviewFilePath := '';
		END_IF
				
	ELSE
		Info.PreviewFilePath := '';
		RoboArmPara.MoveProgramOptions.StartMode := mcPRGSM_SIMULATION; //this run allows generation of path preview before it's actually executed. the program is run once in simulation mode and if there are any errors, it will trigger them the same way as a normal run.
		Info.SimDone := FALSE;
		Info.ClearStream := TRUE;
	END_IF
			
	CASE Info.CurrentState OF				
		autoSTATE_IDLE:
			
			Info.ClearStream := TRUE;
			IF Parameters.JustSim AND Parameters.ProgramName <> '' THEN
				Info.SimDone := FALSE;
				Info.Updated := FALSE;
				Info.FileCopied := FALSE;
				Info.CurrentState := autoSTATE_EXECUTE;
			ELSIF Parameters.Execute AND Parameters.ProgramName <> '' THEN
				Info.SimDone := FALSE;
				Info.Updated := FALSE;
				Info.FileCopied := FALSE;
				Info.CurrentState := autoSTATE_EXECUTE;
			ELSIF Parameters.Continue THEN
				Info.CurrentState := autoSTATE_CONTINUE;
			ELSIF Parameters.Abort THEN
				Info.CurrentState := autoSTATE_ABORT;
			ELSIF Parameters.Pause THEN
				Info.CurrentState := autoSTATE_PAUSE;
			END_IF
			
			
		autoSTATE_EXECUTE: //this state executes a program specified in "ProgramName", if program isn't loaded it will automagically load it
			RoboArm.Interrupt := FALSE;
			RoboArm.Continue := FALSE;
			
			RoboArmPara.ProgramName := Parameters.ProgramName;
			IF NOT Info.Updated THEN
				RoboArm.Update := TRUE;
				IF RoboArm.UpdateDone THEN
					Info.Updated := TRUE;
					RoboArm.Update := FALSE;
				END_IF
			ELSE
				IF Info.SimDone THEN
					IF NOT Info.FileCopied THEN
						NowCopy := TRUE;
					END_IF
					
					IF Parameters.JustSim THEN
						Info.SimDone := FALSE;
						Info.CurrentState := autoSTATE_DONE;
					ELSE
						RoboArmPara.MoveProgramOptions.StartMode := mcPRGSM_NON_MODAL;
					END_IF
					
				ELSIF NOT Info.FileCopied THEN
					
					RoboArmPara.MoveProgramOptions.StartMode := mcPRGSM_SIMULATION;
				END_IF
				
				IF RoboArmPara.MoveProgramOptions.StartMode <> mcPRGSM_SIMULATION THEN
					IF Info.FileCopied AND NOT NowCopy THEN
						RoboArm.MoveProgram := TRUE;
					END_IF
				ELSIF Info.CurrentState <> autoSTATE_DONE THEN
					RoboArm.MoveProgram := TRUE;
				END_IF
				
				IF Parameters.Abort THEN
					Info.CurrentState := autoSTATE_ABORT;
				ELSIF Parameters.Pause THEN
					Info.CurrentState := autoSTATE_PAUSE;
				ELSIF RoboArm.MoveDone THEN
					Info.ClearStream := FALSE;
					RoboArm.MoveProgram := FALSE;
					Info.LastProgram := Parameters.ProgramName;
					IF RoboArmPara.MoveProgramOptions.StartMode = mcPRGSM_SIMULATION THEN
						Info.SimDone := TRUE;
						Info.FileCopied := FALSE;
						TempFlag := FALSE;
						Info.CurrentState := autoSTATE_EXECUTE; //if it's a simulation run, repeat execution.
					ELSE
						Info.SimDone := FALSE;
						Info.CurrentState := autoSTATE_DONE;
					END_IF
				END_IF
				
			END_IF
			
						
		autoSTATE_DONE: //this state is a goto for execute when it's done
			
			Info.ClearStream := TRUE;
			
			IF Parameters.Continuous THEN
				Info.CurrentState := autoSTATE_EXECUTE;
			ELSE
				Parameters.JustSim := FALSE;
				Parameters.Execute := FALSE;
				Info.CurrentState := autoSTATE_IDLE;
			END_IF
						
		autoSTATE_PAUSE: //this state pauses active program execution
			
			IF Parameters.Pause AND RoboArm.InMotion THEN
				RoboArm.Interrupt := TRUE;
			ELSIF RoboArm.MoveInterrupted THEN
				Info.Paused := TRUE;
				Parameters.Execute := FALSE;
				Parameters.Pause := FALSE;
				Info.CurrentState := autoSTATE_IDLE;
			ELSE
				Parameters.Pause := FALSE;
				Info.CurrentState := autoSTATE_IDLE;
			END_IF
								
		autoSTATE_CONTINUE: //this state continues paused program
			
			IF Parameters.Continue THEN
				Info.Paused := FALSE;
				RoboArm.Interrupt := FALSE;
				RoboArm.Continue := TRUE;
				IF RoboArm.InMotion THEN
					RoboArm.Continue := FALSE;
					Parameters.Continue := FALSE;
					Parameters.Execute := TRUE;
					Info.CurrentState := autoSTATE_EXECUTE;
				END_IF
			ELSE
				Info.CurrentState := autoSTATE_IDLE;
			END_IF
						
		autoSTATE_ABORT: //a noncritical stop
			
			Info.PreviewFilePath := '';
			Parameters.Execute := FALSE;
			Parameters.Continue := FALSE;
			Parameters.Pause := FALSE;
			Parameters.JustSim := FALSE;
			Info.SimDone := FALSE;
			RoboArm.Interrupt := FALSE;
			RoboArm.Continue := FALSE;
			Info.Paused := FALSE;
			Info.FileCopied := FALSE;
			TempFlag := FALSE;
			RoboArm.MoveProgram := FALSE;
			RoboArm.Stop := TRUE;
			
			IF RoboArm.Stopped THEN
				Parameters.Abort := FALSE;
				RoboArm.Stop := FALSE;
				Info.CurrentState := autoSTATE_IDLE;
			END_IF
			
		END_CASE
	
	
END_FUNCTION_BLOCK
