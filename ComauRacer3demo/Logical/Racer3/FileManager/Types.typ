
TYPE
	FileManagerUI : 	STRUCT 
		FileManagerUI : MpFileManagerUI;
		MpFileManagerUISetupType : MpFileManagerUISetupType := (FileListSize:=50);
		MpFileManagerUIConnectType : MpFileManagerUIConnectType;
	END_STRUCT;
	TableInfo : 	STRUCT 
		Name : ARRAY[0..49]OF STRING[255];
		Size : ARRAY[0..49]OF UDINT;
		Type : ARRAY[0..49]OF MpFileManagerUIItemTypeEnum;
		LastModified : ARRAY[0..49]OF DATE_AND_TIME;
		NumberOfElements : USINT;
		SelectedRow : USINT;
	END_STRUCT;
	Auxiliary : 	STRUCT 
		Two : STRING[160];
		One : STRING[160];
		TableConfigurationStr : STRING[255];
		NumberOfElementsStr : STRING[80];
		NewNamePlus : USINT;
		NewNamePlusStr : STRING[80];
		NewNameTextOne : STRING[80];
		NewNameTextTwo : STRING[80];
		prevNewName : STRING[255];
		NumberOfElementsMinus : USINT;
		newNewName : STRING[255];
		btnSort : USINT;
		btnSortStr : STRING[80];
		fileName : STRING[260];
		fullFilePath : STRING[260];
		FilePath : STRING[100];
		fullFilePathWithFileDevice : STRING[260];
		IsImage : BOOL;
		IsVideo : BOOL;
		IsPDF : BOOL;
		IsText : BOOL;
		IsScript : BOOL;
		IsSVG : BOOL;
		IsXML : BOOL;
		IsFolder : BOOL;
		SearchNewName : STRING[255];
		textStar : STRING[80] := '*';
		DelayCounter : UINT;
	END_STRUCT;
END_TYPE
