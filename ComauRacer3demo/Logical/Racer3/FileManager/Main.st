
PROGRAM _INIT
	cmd.FileManagerUI.MpLink := ADR(gFileManagerUI);
	cmd.FileManagerUI.Enable := TRUE;

	cmd.FileManagerUI.UISetup := cmd.MpFileManagerUISetupType;
	cmd.FileManagerUI.UIConnect := ADR(cmd.MpFileManagerUIConnectType);

	(* define of device names *) 
	cmd.MpFileManagerUIConnectType.DeviceList.DeviceNames[0] := 'CNC_PrgDir';
	cmd.MpFileManagerUIConnectType.DeviceList.DeviceNames[1] := 'UserPart';	
	cmd.MpFileManagerUIConnectType.DeviceList.DeviceNames[2] := 'AUDIT';	
	cmd.MpFileManagerUIConnectType.DeviceList.DeviceNames[3] := 'RECIPE'; //currently (*not*) implemented
	
	cmd.MpFileManagerUIConnectType.File.NewName := '';
	
	cmd.FileManagerUI();
	
	//aux.Counter := 0;
	//tabInfo.PrevSelectedRow := 255;
	
	aux.DelayCounter := 0;
END_PROGRAM

PROGRAM _CYCLIC
	
	IF aux.DelayCounter = 0 THEN //autorefresh of current folder. Useful. (no more button pressing, in fact, the button for manual refresh has been removed)
		cmd.MpFileManagerUIConnectType.File.Refresh := TRUE;
	ELSE
		cmd.MpFileManagerUIConnectType.File.Refresh := FALSE;
		aux.DelayCounter := aux.DelayCounter + 1;
	END_IF
	
	tabInfo.NumberOfElements := 0;
		FOR iFile := 0 TO 49 BY 1 DO 	
			(* create arrays with file properties *)
			brsmemcpy(ADR(tabInfo.Name[iFile]), ADR(cmd.MpFileManagerUIConnectType.File.List.Items[iFile].Name), SIZEOF(tabInfo.Name[iFile]));
			brsmemcpy(ADR(tabInfo.Size[iFile]), ADR(cmd.MpFileManagerUIConnectType.File.List.Items[iFile].Size), SIZEOF(tabInfo.Size[iFile]));
			brsmemcpy(ADR(tabInfo.Type[iFile]), ADR(cmd.MpFileManagerUIConnectType.File.List.Items[iFile].ItemType), SIZEOF(tabInfo.Type[iFile]));
			brsmemcpy(ADR(tabInfo.LastModified[iFile]), ADR(cmd.MpFileManagerUIConnectType.File.List.Items[iFile].LastModified), SIZEOF(tabInfo.LastModified[iFile]));
			
			IF tabInfo.SelectedRow = iFile THEN
				cmd.MpFileManagerUIConnectType.File.List.Items[iFile].IsSelected := TRUE;
			END_IF
			
			(* numbers of elements in the table *)
			IF tabInfo.Type[iFile] <> mpFILE_ITEM_TYPE_NONE THEN
				tabInfo.NumberOfElements := tabInfo.NumberOfElements + 1;
			END_IF
		END_FOR
		
	(* dynamic change of the array size *)
	brsitoa(tabInfo.NumberOfElements, ADR(aux.NumberOfElementsStr));
	aux.TableConfigurationStr := '{"specRows": [ {"from":0, "to":';
	aux.One := ', "visible":true}, {"from":';
	aux.Two := ', "to":49, "visible":false}]}';
	brsstrcat(ADR(aux.TableConfigurationStr), ADR(aux.NumberOfElementsStr));
	brsstrcat(ADR(aux.TableConfigurationStr), ADR(aux.One));
	brsstrcat(ADR(aux.TableConfigurationStr), ADR(aux.NumberOfElementsStr));
	brsstrcat(ADR(aux.TableConfigurationStr), ADR(aux.Two));
	
	
	aux.NumberOfElementsMinus := tabInfo.NumberOfElements - 1;

	IF tabInfo.NumberOfElements >= 1 THEN
		aux.NewNamePlus := 0;
		aux.newNewName := '';
		aux.prevNewName := cmd.MpFileManagerUIConnectType.File.NewName;
		aux.newNewName := aux.prevNewName;
		FOR iFile := 0 TO aux.NumberOfElementsMinus BY 1 DO
			(* dynamic change of the existing name *)
			IF tabInfo.Name[iFile] = aux.newNewName THEN
				aux.newNewName := aux.prevNewName;
				aux.NewNamePlus := aux.NewNamePlus + 1;
				brsitoa(aux.NewNamePlus, ADR(aux.NewNamePlusStr));
				aux.NewNameTextOne := '(';
				aux.NewNameTextTwo := ')';
				brsstrcat(ADR(aux.newNewName), ADR(aux.NewNameTextOne));
				brsstrcat(ADR(aux.newNewName), ADR(aux.NewNamePlusStr));
				brsstrcat(ADR(aux.newNewName), ADR(aux.NewNameTextTwo));
				//iFile := 0;	
			ELSE
				cmd.MpFileManagerUIConnectType.File.NewName := aux.newNewName;
			END_IF
		END_FOR
	END_IF
		
	(* search engine *)
	cmd.MpFileManagerUIConnectType.File.Filter := '*';
	IF aux.SearchNewName <> '' THEN
		brsstrcat(ADR(cmd.MpFileManagerUIConnectType.File.Filter), ADR(aux.SearchNewName));
		brsstrcat(ADR(cmd.MpFileManagerUIConnectType.File.Filter), ADR(aux.textStar));
		cmd.MpFileManagerUIConnectType.File.Refresh := TRUE;
	ELSE
		cmd.MpFileManagerUIConnectType.File.Filter := '*';
	END_IF
	
	//change sorting method
	CASE aux.btnSort OF
		0: 
			cmd.MpFileManagerUIConnectType.File.SortOrder := mpFILE_SORT_BY_NAME_ASC;
			aux.btnSortStr := '$$IAT/Browser/SortByNameAsc'; 
		1: 
			cmd.MpFileManagerUIConnectType.File.SortOrder := mpFILE_SORT_BY_NAME_DESC;
			aux.btnSortStr := '$$IAT/Browser/SortByNameDesc'; 
		2: 
			cmd.MpFileManagerUIConnectType.File.SortOrder := mpFILE_SORT_BY_SIZE_ASC;
			aux.btnSortStr := '$$IAT/Browser/SortBySizeAsc'; 
		3: 
			cmd.MpFileManagerUIConnectType.File.SortOrder := mpFILE_SORT_BY_SIZE_DES;
			aux.btnSortStr := '$$IAT/Browser/SortBySizeDesc'; 
		4: 
			cmd.MpFileManagerUIConnectType.File.SortOrder := mpFILE_SORT_BY_MOD_TIME_ASC;
			aux.btnSortStr := '$$IAT/Browser/SortByLastModAsc'; 
		5: 
			cmd.MpFileManagerUIConnectType.File.SortOrder := mpFILE_SORT_BY_MOD_TIME_DESC;
			aux.btnSortStr := '$$IAT/Browser/SortByLastModDesc'; 
		6:
			aux.btnSort := 0;
	END_CASE
	
	//variables used to load files
	IF brsstrcmp(ADR(aux.fileName), ADR(tabInfo.Name[tabInfo.SelectedRow])) <> 0 THEN //if a change in current file is detected - optimalisation
		aux.fileName := tabInfo.Name[tabInfo.SelectedRow];
		aux.NewNameTextOne := '/';
		aux.fullFilePath := '';
		aux.FilePath := '';
		aux.fullFilePathWithFileDevice := '/FileDevice:';
		
		IF brsstrcmp(ADR(aux.fullFilePath), ADR(cmd.MpFileManagerUIConnectType.File.PathInfo.CurrentDir)) = 0 THEN //subfolder compatibility (check if currentdir is null)
			brsstrcat(ADR(aux.fullFilePath), ADR(cmd.MpFileManagerUIConnectType.DeviceList.DeviceNames[cmd.MpFileManagerUIConnectType.DeviceList.SelectedIndex])); //device name
			brsstrcat(ADR(aux.fullFilePath), ADR(aux.NewNameTextOne)); // '/'
			brsstrcat(ADR(aux.fullFilePath), ADR(aux.fileName)); // file name, this gives full file path in subsystem.
			
			//FilePath - for recipes
			brsstrcat(ADR(aux.FilePath), ADR(aux.fileName)); //just the file name in FilePath
		ELSE
			brsstrcat(ADR(aux.fullFilePath), ADR(cmd.MpFileManagerUIConnectType.DeviceList.DeviceNames[cmd.MpFileManagerUIConnectType.DeviceList.SelectedIndex])); //device name
			brsstrcat(ADR(aux.fullFilePath), ADR(aux.NewNameTextOne)); // '/'
			brsstrcat(ADR(aux.fullFilePath), ADR(cmd.MpFileManagerUIConnectType.File.PathInfo.CurrentDir)); //this adds subfolder compatibility
			brsstrcat(ADR(aux.fullFilePath), ADR(aux.NewNameTextOne)); // '/'
			brsstrcat(ADR(aux.fullFilePath), ADR(aux.fileName)); // file name, this gives full file path in subsystem.
			
			//FilePath - for recipes
			brsstrcat(ADR(aux.FilePath), ADR(cmd.MpFileManagerUIConnectType.File.PathInfo.CurrentDir));
			brsstrcat(ADR(aux.FilePath), ADR(aux.NewNameTextOne)); // '/'
			brsstrcat(ADR(aux.FilePath), ADR(aux.fileName)); // file name, this gives file path in subsystem.
		END_IF
		
		brsstrcat(ADR(aux.fullFilePathWithFileDevice), ADR(aux.fullFilePath)); //this gives variable with /FileDevice: prefix, used for PDFs and images (file loading).
		
		aux.IsVideo := FALSE;
		aux.IsImage := FALSE;
		aux.IsPDF := FALSE;
		aux.IsSVG := FALSE;
		aux.IsScript := FALSE;
		aux.IsText := FALSE;
		aux.IsFolder := FALSE;
		aux.IsXML := FALSE;
		
		CASE tabInfo.Type[tabInfo.SelectedRow] OF
			mpFILE_ITEM_TYPE_FOLDER:
				aux.IsFolder := TRUE;
			mpFILE_ITEM_TYPE_XML:
				IF cmd.MpFileManagerUIConnectType.DeviceList.SelectedIndex = 3 THEN
					aux.IsXML := TRUE;
				END_IF
				
			mpFILE_ITEM_TYPE_USER3:
				aux.IsPDF := TRUE;
			mpFILE_ITEM_TYPE_USER4:
				aux.IsSVG := TRUE;
			mpFILE_ITEM_TYPE_USER1, mpFILE_ITEM_TYPE_USER2:
				aux.IsScript := TRUE;
				aux.IsText := TRUE;
			mpFILE_ITEM_TYPE_TXT:
				aux.IsText := TRUE;
			mpFILE_ITEM_TYPE_JPG, mpFILE_ITEM_TYPE_BMP:
				aux.IsImage := TRUE;
			mpFILE_ITEM_TYPE_USER5: //currently only .mp4 files are programmed in
				aux.IsVideo := TRUE;
		END_CASE
	END_IF
	
		
	cmd.FileManagerUI();
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	cmd.FileManagerUI(Enable := FALSE);
END_PROGRAM

