
PROGRAM _INIT
 
	gdoAcoposEnable1;
	gdoAcoposEnable2;
	
	EStop;
	EStopPanel;
	GuardDoor;
	
	diEStop;
	diEStopPanel;
	diGuardDoor1;
	diGuardDoor2;
	
	SafeOutputOk1;
	SafeOutputOk2;
	
END_PROGRAM


PROGRAM _CYCLIC
	
	gdiErrorResetButton;
	
END_PROGRAM

PROGRAM _EXIT
	 
END_PROGRAM

