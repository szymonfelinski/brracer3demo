
PROGRAM _INIT
	(* Insert code here *)
	RecipeXML(MpLink := ADR(gRecipeXml) , Enable := TRUE, DeviceName := ADR('RECIPE'), FileName := ADR(RecipeFile));
	RecipeRegPar(MpLink := ADR(gRecipeXml), Enable := TRUE, PVName := ADR('gRecipe'));
	gRecipe;
	RecipeFile;
END_PROGRAM

PROGRAM _CYCLIC
	(* Insert code here *)
	
	IF RecipeXML.CommandDone THEN
		RecipeXML.Load := FALSE;
		RecipeXML.Save := FALSE;
	END_IF
	
	RecipeXML(MpLink := ADR(gRecipeXml) , Enable := TRUE, DeviceName := ADR('RECIPE'), FileName := ADR(RecipeFile));
	RecipeRegPar(MpLink := ADR(gRecipeXml), Enable := TRUE, PVName := ADR('gRecipe'));
	//RecipeRegPar(MpLink := , Enable := , ErrorReset := , PVName := , Category := );
	//RecipeUI(MpLink := , Enable := , ErrorReset := , UISetup := , UIConnect := );
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

