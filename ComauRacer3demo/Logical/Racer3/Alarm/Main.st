
PROGRAM _INIT
	
	AlarmXCore(MpLink := ADR(gAlarmXCoreLink) , Enable := TRUE);
	AlarmXHistory(MpLink := ADR(gAlarmXHistoryLink), DeviceName := ADR('AUDIT'), Enable := TRUE);
	
	GroupReadError(AxesGroup := ADR(gAxesGroupRacer3), Enable := TRUE);
	
	AlarmStep := 1;
	
END_PROGRAM

PROGRAM _CYCLIC
	//Reading axes group errors - works
	GroupReadError();
	IF GroupReadError.GroupErrorID <> 0 THEN
		IF ErrorID <> GroupReadError.GroupErrorID THEN
			ErrorID := GroupReadError.GroupErrorID;
			TriggerAxesGroupAlarm := TRUE;
		ELSIF NOT GroupReadError.ReadNext THEN
			GroupReadError.ReadNext := TRUE;
		ELSE
			GroupReadError.ReadNext := FALSE;
		END_IF
		
	END_IF
	
	IF AlarmClearHistory THEN
		MpAlarmXClearHistory(MpLink := gAlarmXHistoryLink); //doesn't seem to work (29.10.21)
	END_IF
		
	//AlarmXCore(Enable := TRUE);
	//AlarmXHistory(Enable := TRUE);
	
	//TEST ALARM
	IF TriggerAxesGroupAlarm THEN
		MpAlarmXSet(MpLink := gAlarmXCoreLink, Name := 'AxesGroupError');
		TriggerAxesGroupAlarm := FALSE;
	END_IF
	
	IF AlarmXQuery.Error THEN
		AlarmStep := 4;
	END_IF
		
	CASE AlarmStep OF
		1:
			
			AlarmXQuery.Execute := TRUE;
			IF AlarmXQuery.CommandBusy THEN
				AlarmStep := 2;
			END_IF
			
		2:
			
			IF AlarmXQuery.CommandDone THEN
				AlarmStep := 3;
			END_IF
			
		3:
			
			AlarmXQuery.Execute := FALSE;
			AlarmStep := 1;
			
		4:
			
			AlarmXQuery.Execute := FALSE;
			AlarmXQuery.ErrorReset := TRUE;
			IF NOT AlarmXQuery.Error THEN
				AlarmStep := 1;
			END_IF
		
	END_CASE
	
	AlarmXQuery(Enable := TRUE, MpLink := ADR(gAlarmXCoreLink), Name := ADR('AnyAlarms'));
	
END_PROGRAM

PROGRAM _EXIT
	
	AlarmXCore(Enable := FALSE);
	AlarmXHistory(Enable := FALSE);
	 
END_PROGRAM

