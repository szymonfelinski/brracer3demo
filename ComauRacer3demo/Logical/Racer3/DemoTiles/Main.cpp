
#include <bur/plctypes.h>
#include <bur/plc.h>
#include <new>
#include <cstring>
#include <cstdlib>
#include <string>
#include <sstream>
#include "AsBrStr.h"

#define SSTR( x ) static_cast< std::ostringstream & >( \
		( std::ostringstream() << x ) ).str()

#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

//#define HEIGHT 45.0;

// amount of memory to be allocated for heap storage must be specified for every ANSI C++ program with the bur_heap_size variable
unsigned long bur_heap_size = 0xFFFFFF;

std::string svg_header = "";
std::string svg_body = "";
std::string svg_end = "";

float SCALING = 560.0/900.0; // widget size divided by workspace size

float touch_position[4] = {0.0, 0.0, 0.0, 0.0};
short int touch_counter = 0;
//unsigned short int current_tile_status[10];

int tiles[10][5]; //x, y, w, h, ordered number
bool tile_status[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

int findMaxOrderedNumber()
{
	int maxNumber = 0;
	for (int i = 0; i < 10; i++)
	{
		if (tiles[i][4] > maxNumber)
		{
			maxNumber = tiles[i][4];
		}
	}
	test = maxNumber;
	return maxNumber;
}

void sortMoves(int *tmparray, int maxNumber)
{
	for (int i = 0; i < maxNumber; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			if ((i + 1) == tiles[j][4])
			{
				tmparray[i] = j;
				test = tmparray[i];
				break;
			}
		}
	}
}

constexpr int get_five() {return 5;} // C++11 test.

void generateGCode()
{
	int some_value[get_five() + 7];
	std::string moveType = "G00";
	std::string completeGCode = "def LREAL height = " + SSTR(moveHeight);
	std::string tempMove = "";
	int maxMoveNumber = findMaxOrderedNumber();
	
	int sortedMoves[maxMoveNumber];
	
	std::memset(&sortedMoves[0], 0, maxMoveNumber * sizeof(int));
	
	sortMoves(sortedMoves, maxMoveNumber);
	//for (int i = 0; i < maxMoveNumber;i++)
	//	test = sortedMoves[i];
	
	for (int i = 0; i < maxMoveNumber; i++)
	{
		tempMove = "";
		tempMove += "\n" + moveType;
		tempMove += " X" + SSTR(tiles[sortedMoves[i]][1] + (tiles[sortedMoves[i]][2] / 2));
		tempMove += " Y" + SSTR(tiles[sortedMoves[i]][0] + (tiles[sortedMoves[i]][3] / 2));
		tempMove += " Z=height A0 B180 C0";
		completeGCode += tempMove;
		std::memset(&tempMove[0], 0, tempMove.length());
	}
	std::memcpy(&moveset, &completeGCode[0], completeGCode.length());
}

std::string genRect(int x, int y, int w, int h, int number, bool status) // Rectangle drawing function
{
	std::string x_str = SSTR(x * SCALING); //Everything is scaled to fit on the widget.
	std::string y_str = SSTR(y * SCALING);
	std::string w_str = SSTR(w * SCALING);
	std::string h_str = SSTR(h * SCALING);
	std::string fill_str;
	std::string number_text = "<text x=\"" + SSTR((x + w / 2.0) * SCALING) + "\" y=\"" + SSTR((y + h / 2.0) * SCALING) + "\" fill=\"white\">" + SSTR(number) + "</text>";
	if (status)
		fill_str = "black";
	else
		fill_str = "none";
	
	if (number == 0)
		return "<rect x=\"" + x_str + "\" y=\"" + y_str + "\" width=\"" + w_str + "\" height=\"" + h_str +"\" fill=\"" + fill_str + "\" stroke=\"black\" stroke-width=\"2\"/>";
	else
		return "<rect x=\"" + x_str + "\" y=\"" + y_str + "\" width=\"" + w_str + "\" height=\"" + h_str +"\" fill=\"" + fill_str + "\" stroke=\"black\" stroke-width=\"2\"/>" + number_text;
}

std::string drawDot()
{
	if (immediateExecute)
	{
		demo_move[0] = touch_position[1];
		demo_move[1] = touch_position[0];
		demo_move[2] = moveHeight; //height defined here
		demo_move[3] = 0.0;
		demo_move[4] = 180.0;
		demo_move[5] = 0.0;
		demo_move[6] = 15.0; //this array element is used in Control::Main.st for command translation.
	}
	
	std::string x_str = SSTR(touch_position[0] * SCALING);
	std::string y_str = SSTR(touch_position[1] * SCALING);
	return "<circle cx=\"" + x_str + "\" cy=\"" + y_str + "\" r=\"5\" fill=\"red\"/>";
}

std::string drawTiles()
{
	std::string tiles_body = "";
	for (int i = 0; i < 10; i++)
	{
		tiles_body += genRect(tiles[i][0], tiles[i][1], tiles[i][2], tiles[i][3], tiles[i][4], tile_status[i]);
	}
	return tiles_body;
}

void generate_tiles()
{
	//tiles[0][0] = 115;  // Initial position 100mm + 115 mm
	for (int i = 0; i < 10; i++)
	{
		if (i == 0)
		{
			tiles[i][0] = 215;
			tiles[i][1] = 700;
		}
		else if (i < 5)
		{
			tiles[i][0] = tiles[i-1][0] + 100;
			tiles[i][1] = 700;
		}
		else if (i == 5)
		{
			tiles[i][0] = tiles[i-1][0];
			tiles[i][1] = 600;
		}
		else
		{
			tiles[i][0] = tiles[i-1][0] - 100;
			tiles[i][1] = 600;
		}
		tiles[i][2] = 100; // size of tiles. 100x100 mm default.
		tiles[i][3] = 100;
		
	}
}

void parse_tile_numbers(int removed_number) //shift all numbers higher than desired number by -1 to avoid conflicts and ensure consistency
{
	for (int i = 0; i < 10; i++)
	{
		tiles[i][4] > removed_number ? tiles[i][4]-- : tiles[i][4];
	}
}

void check_tile_touch()
{
	for (int i = 0; i < 10; i++)
	{
		if (touch_position[0] > tiles[i][0] && touch_position[1] > tiles[i][1] && touch_position[0] < tiles[i][0] + tiles[i][2] && touch_position[1] < tiles[i][1] + tiles[i][3]) //these expressions check if the touch occured inside a tile
		{
			tile_status[i] ? touch_counter-- : touch_counter++; //change touch counter for tile counting
			tile_status[i] = !tile_status[i]; //invert tile status
				
			if (tile_status[i]) //if new status is true then write current touch counter to the tile's flag
			{
				tiles[i][4] = touch_counter;
			}
			else
			{
					
				parse_tile_numbers(tiles[i][4]); // correct numbers to ensure consistency
				tiles[i][4] = 0;
			}
		}
		
	}
}


bool detect_touch()
{
	touch_position[0] = visClickX / SCALING;
	touch_position[1] = visClickY / SCALING;
	if (touch_position[2] != touch_position[0] || touch_position[3] != touch_position[1])
	{
		touch_position[2] = touch_position[0]; //touch memory
		touch_position[3] = touch_position[1];
		return true;
	}
	else 
		return false;
}

void _INIT ProgramInit(void)
{
	//std::memset(&tiles[0], 0, sizeof(int)*10*5);
	generate_tiles();
	
	int paper_width = 560;
	int paper_height = 560;
	std::string paper_width_str = SSTR(paper_width);
	std::string paper_height_str = SSTR(paper_height);
	
	svg_header = "<?xml version=\"1.0\" standalone=\"no\"?><svg width=\"";
	
	svg_header = svg_header + paper_width_str + "\" height=\"" + paper_height_str + "\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">";
	
	svg_end = "</svg>";
	// Insert code here 

}

void _CYCLIC ProgramCyclic(void)
{
	std::memset(&svg_body[0], 0, svg_body.length());
	svg_body = " "; 					// String needs to be initialized after memory wipe.
	
	if (primeRobot)
	{
		demo_move[6] = -11.0;
		primeRobot = 0;
	}
	else if (saveMove)
	{
		std::memset(&moveset, 0, sizeof(moveset));
		generateGCode();
		demo_move[6] = -100.0;
		saveMove = 0;
	}
	else if (detect_touch())
	{
		svg_body += drawDot();
		check_tile_touch();
	}
	
	svg_body += drawTiles();
	
	std::string tmp_str = svg_header + svg_body + svg_end;
	
	std::memset(&tiles_svg, 0, sizeof(tiles_svg));
	std::memcpy(&tiles_svg, &tmp_str[0], tmp_str.length());
	
	//std::memset(&sss, 0, sizeof(sss));
	//std::memcpy(&sss, &svg_body[0], svg_body.length());
}

void _EXIT ProgramExit(void)
{
	// Insert code here 

}
