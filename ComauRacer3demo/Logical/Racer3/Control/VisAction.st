
ACTION VisAction:
	
	IF Communication.CoordinateSystemSemiAuto = 9 THEN
		
		Communication.SemiAutoCoordButtons[0] := TRUE;
		Communication.SemiAutoCoordButtons[1] := FALSE;
		Communication.SemiAutoCoordButtons[2] := FALSE;
		
	ELSIF Communication.CoordinateSystemSemiAuto = 10 THEN
		
		Communication.SemiAutoCoordButtons[0] := FALSE;
		Communication.SemiAutoCoordButtons[1] := TRUE;
		Communication.SemiAutoCoordButtons[2] := FALSE;
		
	ELSIF Communication.CoordinateSystemSemiAuto = 0 THEN
		
		Communication.SemiAutoCoordButtons[0] := FALSE;
		Communication.SemiAutoCoordButtons[1] := FALSE;
		Communication.SemiAutoCoordButtons[2] := TRUE;
		
	END_IF
		
	IF Calibration.Para.Parameters.AxisCalibrated[0] AND Calibration.Para.Parameters.AxisCalibrated[1] AND Calibration.Para.Parameters.AxisCalibrated[2] AND Calibration.Para.Parameters.AxisCalibrated[3] THEN
		Axis_1to4_calibrated := TRUE;
	ELSE
		Axis_1to4_calibrated := FALSE;
	END_IF
	
	IF Axis_1to4_calibrated AND Calibration.Para.Parameters.AxisCalibrated[4] THEN
		Axis5Calibrated := TRUE;
	ELSE
		Axis5Calibrated := FALSE;
	END_IF
	
	(* comedy *)
	IF KeyCheck.value_changed THEN
		KeyCheck.pending_combo[4] := KeyCheck.pending_combo[3];
		KeyCheck.pending_combo[3] := KeyCheck.pending_combo[2];
		KeyCheck.pending_combo[2] := KeyCheck.pending_combo[1];
		KeyCheck.pending_combo[1] := KeyCheck.pending_combo[0];
		KeyCheck.pending_combo[0] := KeyCheck.temp_value;
		
		IF KeyCheck.pending_combo[4] = KeyCheck.original_combo[4] AND KeyCheck.pending_combo[3] = KeyCheck.original_combo[3] AND KeyCheck.pending_combo[2] = KeyCheck.original_combo[2] AND KeyCheck.pending_combo[1] = KeyCheck.original_combo[1] AND KeyCheck.pending_combo[0] = KeyCheck.original_combo[0] THEN
			KeyCheck.event_achieved := TRUE;
		ELSE
			KeyCheck.event_achieved := FALSE;
		END_IF
						
		KeyCheck.value_changed := FALSE;
	END_IF
	
	IF Calibration.Para.Parameters.CalibrationModePara.AxisButton.Q1 THEN
		Communication.CalibrationImage := 'Media/calibration/calib_axis1.PNG';
		Communication.TextManager.CalibHelper := '$$IAT/ControlPage/Calibration.Helper.1';
	ELSIF Calibration.Para.Parameters.CalibrationModePara.AxisButton.Q2 THEN
		Communication.CalibrationImage := 'Media/calibration/calib_axis2.PNG';
		Communication.TextManager.CalibHelper := '$$IAT/ControlPage/Calibration.Helper.2';
	ELSIF Calibration.Para.Parameters.CalibrationModePara.AxisButton.Q3 THEN
		Communication.CalibrationImage := 'Media/calibration/calib_axis3.PNG';
		Communication.TextManager.CalibHelper := '$$IAT/ControlPage/Calibration.Helper.3';
	ELSIF Calibration.Para.Parameters.CalibrationModePara.AxisButton.Q4 THEN
		Communication.CalibrationImage := 'Media/calibration/calib_axis4.PNG';
		Communication.TextManager.CalibHelper := '$$IAT/ControlPage/Calibration.Helper.4';
	ELSIF Calibration.Para.Parameters.CalibrationModePara.AxisButton.Q5 THEN
		Communication.CalibrationImage := 'Media/calibration/calib_axis5.PNG';
		Communication.TextManager.CalibHelper := '$$IAT/ControlPage/Calibration.Helper.5';
	ELSIF Calibration.Para.Parameters.CalibrationModePara.AxisButton.Q6 THEN
		Communication.CalibrationImage := 'Media/calibration/calib_axis6.PNG';
		Communication.TextManager.CalibHelper := '$$IAT/ControlPage/Calibration.Helper.6';
	ELSE
		Communication.CalibrationImage := '';
		Communication.TextManager.CalibHelper := '';
	END_IF
	
	IF Calibration.Para.Parameters.AxisCalibrated[0] AND Calibration.Para.Parameters.AxisCalibrated[1] AND Calibration.Para.Parameters.AxisCalibrated[2] AND Calibration.Para.Parameters.AxisCalibrated[3] AND Calibration.Para.Parameters.AxisCalibrated[4] AND Calibration.Para.Parameters.AxisCalibrated[5] AND NOT gIsCalibrated THEN
		Communication.TextManager.CalibHelper := '$$IAT/ControlPage/Calibration.Helper.7';
		Communication.RestoreAxis5 := TRUE;
	ELSIF gIsCalibrated THEN
		Communication.TextManager.CalibHelper := '$$IAT/ControlPage/Calibration.Helper.8';
		Communication.RestoreAxis5 := FALSE;
	ELSE 
		Communication.RestoreAxis5 := FALSE;
	END_IF
	
	IF Communication.CalibrateAgain THEN
		
		gIsCalibrated := FALSE;
		Calibration.Para.Parameters.AxisCalibrated[0] := FALSE;
		Calibration.Para.Parameters.AxisCalibrated[1] := FALSE;
		Calibration.Para.Parameters.AxisCalibrated[2] := FALSE;
		Calibration.Para.Parameters.AxisCalibrated[3] := FALSE;
		Calibration.Para.Parameters.AxisCalibrated[4] := FALSE;
		Calibration.Para.Parameters.AxisCalibrated[5] := FALSE;
		Communication.CalibrateAgain := FALSE;
	END_IF
	
	
END_ACTION
