
ACTION AxisLimitsAction: 
	RoboArm6Axis.Power := FALSE;
	IF NOT RoboArm6Axis.PowerOn THEN
		Calibration.Cmds.ProcessConfig.DataType := 10012;
		Calibration.Cmds.ProcessConfig.DataAddress := ADR(Calibration.Para.AxisLim);
		Calibration.Cmds.ProcessConfig.ExecutionMode := mcEM_IMMEDIATELY;
		Calibration.Cmds.ProcessConfig.Component := ADR(gAxisQ5);
		Calibration.Cmds.ProcessConfig.Execute := TRUE;
					
		IF Calibration.Info.Axis5LimitsRead AND NOT Calibration.Info.Axis5LimitsSet THEN
			Calibration.Para.AxisLim.MovementLimits.InternalPathAxis.Position.LowerLimit := -118.0;
			Calibration.Cmds.ProcessConfig.Mode := mcPPM_WRITE;
		ELSIF Calibration.Info.Axis5LimitsSet THEN
			Calibration.Para.AxisLim.MovementLimits.InternalPathAxis.Position.LowerLimit := -105.0;
			Calibration.Cmds.ProcessConfig.Mode := mcPPM_WRITE;
		ELSE
			Calibration.Cmds.ProcessConfig.Mode := mcPPM_READ;
		END_IF
					
		IF Calibration.Cmds.ProcessConfig.Done THEN
			Calibration.Cmds.ProcessConfig.Execute := FALSE;
			IF Calibration.Info.Axis5LimitsRead AND NOT Calibration.Info.Axis5LimitsSet THEN
				Calibration.Info.Axis5LimitsSet := TRUE;
				RoboArm6Axis.Power := TRUE;
			ELSIF Calibration.Info.Axis5LimitsSet THEN
				Calibration.Info.Axis5LimitsRead := FALSE;
				Calibration.Info.Axis5LimitsSet := FALSE;
				RoboArm6Axis.Power := TRUE;
			ELSE
				Calibration.Info.Axis5LimitsRead := TRUE;
			END_IF
		END_IF
										
		Calibration.Cmds.ProcessConfig();
					
	END_IF
END_ACTION
