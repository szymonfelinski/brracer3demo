
ACTION StopAction: 
	
	
	Calibration.Para.Parameters.Axis5To6Para.MoveAxis := FALSE;
	Calibration.Para.Parameters.Axis5To6Para.HomeReturn := FALSE;
	Communication.ReturnToZero := FALSE;
	RoboArm6Axis.MoveLinear := FALSE;
	RoboArm6Axis.MoveDirect := FALSE;
	RoboArm6Axis.Jog := FALSE;
	SemiAutoControlPara.Flag := FALSE;
	ManualControlPara.ActivateMove := FALSE;
	RoboArm6Axis.MoveProgram := FALSE;
	ManualControlPara.ExitManual := FALSE;
	AutomaticModePara.Abort := TRUE;
	SemiAutoControlPara.ExitSemiAuto := TRUE;
	TeachingModePara.Para.SemiAutoModePara.ExitSemiAuto := TRUE;
END_ACTION
