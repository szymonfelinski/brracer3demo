
PROGRAM _INIT
	
	//RoboArm6Axis.Enable := TRUE;
	MappMotionConfiguration;
	
	RoboArm6Axis.Override := 100.0; //it's non-100 only for automatic mode
	RoboArm6AxisPara.ProgramName := 'Point.st';    //File stored on FileDevice, default program (Point.st moves TCP to -200, 300, 800)
	RoboArm6Axis.MpLink := ADR(gAxesGroupRacer3);
	RoboArm6Axis.Parameters := ADR(RoboArm6AxisPara);
	RoboArm6AxisPara.Acceleration := 1000.0;
	RoboArm6AxisPara.Deceleration := 1000.0;
	RoboArm6AxisPara.Velocity := 20.0;
	
	Communication.ReturnToZero := FALSE;
	ManualControlPara.PathLimits.Acceleration := 1000.0;
	ManualControlPara.PathLimits.Deceleration := 1000.0;
	ManualControlPara.PathLimits.Velocity := 150.0;
	ManualControlPara.PathLimits.Jerk := 0.0;
	ManualControlPara.Direction := -1;
	
	State := STATE_INIT;
	Communication.Power := FALSE;
	i;
	Brakes;
	svg_stream;
	
	gRacer3HomeRestorePos; //call home restore position variable to init it in memory
	
	RoboArm6Axis();
	
END_PROGRAM

PROGRAM _CYCLIC
	
	IF RoboArm6Axis.Error OR Communication.AlarmQueryResult <> 0 THEN
		State := STATE_ERROR;
	ELSE
		Communication.ErrorReset := FALSE;
		RoboArm6Axis.ErrorReset := FALSE;
	END_IF
		
	IF Communication.Stop THEN
		//stops and goes to standby
		StopAction;
		IF GroupStop.Done THEN
			StopAction;
			GroupStop(AxesGroup := ADR(gAxesGroupRacer3), Execute := FALSE, StopMode := mcSTOPMODE_NO_JERK_LIMIT);
			Communication.Stop := FALSE;
			State := STATE_READY; //go back to STATE_READY after successful stop
		ELSE
			GroupStop(AxesGroup := ADR(gAxesGroupRacer3), Execute := TRUE, StopMode := mcSTOPMODE_NO_JERK_LIMIT); //this stops the axes with respect to jerk limit
			//TODO clear traces from all moves for sure, not only flags (clean them up) !
			
		END_IF
		
	END_IF
	
	IF Communication.WarmRestart THEN
		Communication.Power := FALSE;
		RoboArm6Axis.Power := FALSE;
		
		SYSreset(1,1);
		Communication.WarmRestart := FALSE;
	END_IF
	UpdateCheck;
	
	IF NOT Communication.Power AND NOT (State = STATE_BRAKES) AND NOT (State = STATE_ERROR) THEN //power commands must have no effect in STATE_BRAKES or STATE_ERROR
		RoboArm6Axis.Power := FALSE;
		IF NOT RoboArm6Axis.PowerOn THEN
			State := STATE_INIT;
		END_IF
		
	END_IF
	
	IF Communication.RecipeLoad THEN //recipe is loaded from global variable
		RoboArm6AxisPara.Acceleration := gRecipe.Acceleration;
		RoboArm6AxisPara.Deceleration := gRecipe.Deceleration;
		RoboArm6AxisPara.Velocity := gRecipe.Velocity;
		RoboArm6AxisPara.Jerk := gRecipe.Jerk;
		Communication.RecipeLoad := FALSE;
	END_IF
	
	IF Communication.ToolOutputReset THEN
		Communication.ToolOutput := FALSE;
		Communication.ToolOutputReset := FALSE;
	END_IF
	
	CASE State OF
		STATE_ERROR:
			Communication.txt_State_out := "Error";
			Communication.txt_State_out_front :=  '$$IAT/ControlPage/txt_State_out_front.Error';
			
			Communication.Power := FALSE;
			RoboArm6Axis.Power := FALSE;
			StopAction;
			
			IF NOT RoboArm6Axis.Error THEN //if there's no error, go to INIT
				Communication.ErrorReset := FALSE;
				RoboArm6Axis.ErrorReset := FALSE;
				
				State := STATE_INIT;
				
			ELSIF Communication.ErrorReset THEN
				RoboArm6Axis.ErrorReset := TRUE;
			END_IF
			
			
		STATE_INIT:
			//RoboArm6Axis.Enable := TRUE;
			Communication.txt_State_out := "Initialization";
			Communication.txt_State_out_front :=  '$$IAT/ControlPage/txt_State_out_front.Init';
			ManualControlPara.ExitManual := FALSE;
			(*IF RoboArm6Axis.PowerOn THEN
				Communication.Power := TRUE;
				Communication.BrakesSet := TRUE;
				//State := STATE_POWER_ON;
			ELSE*)
				
			IF NOT Communication.BrakesSet THEN
				SetBrakes.Axis1 := ADR(gAxisQ1);
				SetBrakes.Axis2 := ADR(gAxisQ2);
				SetBrakes.Axis3 := ADR(gAxisQ3);
				SetBrakes.Axis4 := ADR(gAxisQ4);
				SetBrakes.Axis5 := ADR(gAxisQ5);
				SetBrakes.Axis6 := ADR(gAxisQ6);
				SetBrakes();
				IF SetBrakes.Done THEN
					Communication.BrakesSet := TRUE;
				END_IF
				//RoboArm6Axis.Enable := TRUE;
				
			ELSE //if brake parameters have been set
				// brake operation functions (only when robot is powered off!)
				IF Communication.BrakeClose THEN
					i := 1;
					Brakes.Info.State := brakeCLOSE;
					State := STATE_BRAKES;
		
				ELSIF Communication.BrakeOpen THEN
					i := 1;
					Brakes.Info.State := brakeOPEN;
					State := STATE_BRAKES;
		
				ELSIF Communication.BrakeReadStatus THEN
					i := 1;
					Brakes.Info.State := brakeREAD;
					State := STATE_BRAKES;
		
				ELSIF Communication.Power AND NOT (State = STATE_BRAKES) THEN //power commands have no effect in STATE_BRAKES
					RoboArm6Axis.Enable := TRUE;
					State := STATE_POWER_ON;
				END_IF
				
			END_IF
			
		STATE_POWER_ON:
			Communication.txt_State_out := "Power";
			Communication.txt_State_out_front :=  '$$IAT/ControlPage/txt_State_out_front.Power';
			
			IF NOT RoboArm6Axis.PowerOn AND RoboArm6Axis.Info.ReadyToPowerOn THEN
				RoboArm6Axis.Power := TRUE;
			END_IF
			
			IF RoboArm6Axis.PowerOn THEN //Status tells whether the group is really powered on, Enable must be true., second condition applies when a transfer is initiated - no real benefit otherwise
				State := STATE_HOMING;
			END_IF
			
		STATE_HOMING:
			Communication.txt_State_out := "Homing";
			Communication.txt_State_out_front :=  '$$IAT/ControlPage/txt_State_out_front.Homing';
			
			IF gIsCalibrated THEN
				Communication.CalibrationStateBtn := FALSE;
				IF RoboArm6Axis.IsHomed THEN //autohoming after power on
					RoboArm6Axis.Home := FALSE;
					State := STATE_READY;
				ELSE
				
					RoboArm6Axis.Home := TRUE;
					//Calibration.Cmds.GroupHome(AxesGroup := ADR(gAxesGroupRacer3), Execute := TRUE, HomingMode := Calibration.Para.HomingModeEnum);
					(*IF Calibration.Cmds.GroupHome.Done THEN
					State := STATE_READY;
					END_IF*)
					
				END_IF
			ELSE
				Communication.CalibrationStateBtn := TRUE;
				State := STATE_CALIBRATION; //if the system wasn't calibrated before, make it be calibrated
				Calibration.Info.CurrentState := STATE_BEGIN;
				
			END_IF
			
		STATE_READY:
			//changeStatePending so change mode is impossible while in-move (before state comes to ready) for example for auto and semiauto
			
			(*IF Communication.ReadWorkspace OR R3WorkspaceFB.Para.Cmd.Busy THEN
			WorkspaceControl.Cmd.Mode := mcPCM_LOAD;
			R3WorkspaceFB(Para := WorkspaceControl);
			IF R3WorkspaceFB.Para.Done THEN
			Communication.ReadWorkspace := FALSE;
			END_IF
				
			END_IF*)
			ControlSelector := Communication.changeModePending;
			
			IF Communication.Power AND NOT RoboArm6Axis.PowerOn THEN
				RoboArm6Axis.Power := TRUE;
			END_IF
			
			
			IF Communication.changeModePending = 0 THEN
				ControlSelector := Communication.changeModePending;
			END_IF
			
			IF NOT gIsCalibrated THEN
				Communication.CalibrationStateBtn := TRUE;
				State := STATE_CALIBRATION; //this enables going from Ready to Calibration state in case of manual calibration request
				Calibration.Info.CurrentState := STATE_BEGIN;
				
			END_IF
			
			Communication.ReturnToZero := FALSE; //reset ReturnToZero flag each time manual mode is exit
			(* TODO: Better handling of fake home, do it per axis after each save - DONE*)
			IF NOT Communication.ResetHome THEN //if ResetHome is set off
				IF ControlSelector = ManualJog THEN
					MpAuditWStringChange(MpLink :=  gAuditTrailLink, Old := Communication.txt_State_out ,New := "ManualControl", Identifier := 'ControlMode');
					
					State := STATE_MANUAL_CONTROL;
				ELSIF ControlSelector = SemiAutomatic THEN
					MpAuditWStringChange(MpLink :=  gAuditTrailLink, Old  := Communication.txt_State_out,New := "SemiAutoControl", Identifier := 'ControlMode');
					State := STATE_SEMI_AUTOMATIC;
				ELSIF ControlSelector = Automatic THEN
					MpAuditWStringChange(MpLink :=  gAuditTrailLink, Old  := Communication.txt_State_out,New := "AutoControl", Identifier := 'ControlMode');
					State := STATE_AUTOMATIC;
				ELSIF ControlSelector = Teaching THEN
					MpAuditWStringChange(MpLink :=  gAuditTrailLink, Old  := Communication.txt_State_out,New := "TeachingMode", Identifier := 'ControlMode');
					State := STATE_TEACHING;
				ELSIF ControlSelector = DemoTiles THEN
					MpAuditWStringChange(MpLink :=  gAuditTrailLink, Old  := Communication.txt_State_out,New := "DemoTilesMode", Identifier := 'ControlMode');
					State := STATE_DEMO_TILES;
				ELSE 
					Communication.txt_State_out := "Ready";
					Communication.txt_State_out_front :=  '$$IAT/ControlPage/txt_State_out_front.Ready';
				END_IF
			ELSE
				Calibration.Para.Parameters.Axis5To6Para.Axis3Moved := 0;
				Calibration.Para.Parameters.Axis5To6Para.Axis5Moved := 0;
				Calibration.Para.Parameters.AxisCalibrated[0] := 0;
				Calibration.Para.Parameters.AxisCalibrated[1] := 0;
				Calibration.Para.Parameters.AxisCalibrated[2] := 0;
				Calibration.Para.Parameters.AxisCalibrated[3] := 0;
				Calibration.Para.Parameters.AxisCalibrated[4] := 0;
				Calibration.Para.Parameters.AxisCalibrated[5] := 0;
				State := STATE_CALIBRATION;
			END_IF
			
			
			// TODO think what happens when ControlSelector changes in-between init,update,start,go in auto and semiauto and fix it
			// maybe do something similar to exit mode in manual
		
		STATE_MANUAL_CONTROL:

			IF ControlSelector = ManualJog AND Communication.changeModePending <> 1 THEN
				// exit from MANUAL
				IF NOT RoboArm6Axis.MoveActive THEN //wait for the current command to finish execution (robot stopped)
					ControlSelector := Communication.changeModePending;
					State := STATE_READY;
				ELSE
					ManualControlPara.ExitManual := TRUE;
				END_IF
			END_IF
			IF Communication.Stop THEN
				ManualControlPara.ExitManual := TRUE;
				Communication.Stop := FALSE;
			END_IF
		
			Communication.txt_State_out := "ManualControl";
			Communication.txt_State_out_front := '$$IAT/ControlPage/txt_State_out_front.Manual';
			RoboArm6Axis.Override := 100.0; //required for Jog
			ManualControlPara.PathLimits.Velocity := 1000.0;
			
			IF Communication.ReturnToZero AND ManualControlPara.JogVelocity >= 1.0 AND NOT RoboArm6Axis.Stopped THEN //return to Zero function, only works when velocity is greater than 1.0 (otherwise move could not be executed)
				RoboArm6Axis.Stop := FALSE;
				IF NOT RoboArm6Axis.Stopped THEN
					RoboArm6AxisPara.Position := gRacer3ZeroPosition; //read from PV set during calibration
					RoboArm6AxisPara.CoordSystem := 0;
					RoboArm6AxisPara.ManualMoveType := mcMOVE_ABSOLUTE;
					RoboArm6AxisPara.Velocity := ManualControlPara.JogVelocity;
					RoboArm6Axis.MoveDirect := TRUE;
					IF RoboArm6Axis.MoveDone THEN
						RoboArm6Axis.MoveDirect := FALSE;
						Communication.ReturnToZero := FALSE;
					END_IF
				END_IF
				
			ELSE
				Communication.ToolOutput := ManualControlPara.ToolOutput;
				Communication.ReturnToZero := FALSE;
				RoboArm6Axis.MoveDirect := FALSE;
				ManualControl(ManualModePara := ManualControlPara, RoboArm := RoboArm6Axis, RoboArmPara := RoboArm6AxisPara);
			END_IF
			
		STATE_SEMI_AUTOMATIC:
			
			//ChangeMode
			IF ControlSelector = SemiAutomatic AND Communication.changeModePending <> 2 THEN
				//soft exit from SemiAuto
				IF NOT RoboArm6Axis.MoveActive THEN //wait for the current command to finish execution (robot stopped)
					ControlSelector := Communication.changeModePending;
					State := STATE_READY;
				END_IF
			END_IF
			
			Communication.txt_State_out  := "SemiAutoControl";
			Communication.txt_State_out_front := '$$IAT/ControlPage/txt_State_out_front.SemiAuto';
			RoboArm6Axis.Override := 100.0;
			SemiAutoControl(SemiAutoModePara := SemiAutoControlPara, RoboArm := RoboArm6Axis, RoboArmPara := RoboArm6AxisPara);
		
		STATE_TEACHING:
			
			IF ControlSelector = Teaching AND Communication.changeModePending <> 4 THEN
				//soft exit from Teaching mode
				IF NOT RoboArm6Axis.MoveActive THEN //wait for the current command to finish execution (robot stopped)
					ControlSelector := Communication.changeModePending;
					State := STATE_READY;
				END_IF
			END_IF
			
			Communication.txt_State_out  := "Teaching";
			Communication.txt_State_out_front := '$$IAT/ControlPage/txt_State_out_front.TeachingMode';
			RoboArm6Axis.Override := 100.0;
			
			TeachingMode(Para := TeachingModePara, RoboArm := RoboArm6Axis, RoboArmPara := RoboArm6AxisPara);
			
			Communication.ToolOutput := TeachingModePara.Para.ToolOutput;
			
			IF TeachingModePara.Info.FileStatus <> 0 THEN //FileIO error code display
				MpAlarmXSet(MpLink := gAlarmXCoreLink, Name := 'FileIOError');
				TeachingModePara.Info.FileStatus := 0;
			END_IF
			
		STATE_AUTOMATIC:
			//Communication.LastControlState := 'Automatic';
			Communication.txt_State_out := "AutoControl";
			Communication.txt_State_out_front := '$$IAT/ControlPage/txt_State_out_front.Auto';
			
			IF ControlSelector = Automatic AND Communication.changeModePending <> 3 THEN
				// soft exit from Auto
				IF NOT AutomaticModePara.Execute THEN //wait for the current program to finish execution (robot stopped)
					ControlSelector := Communication.changeModePending;
					State := STATE_READY;
				END_IF
			END_IF
			
			IF AutomaticModePara.ToolOutputReset THEN //the M17 G-Code function operation
				Communication.ToolOutputReset := TRUE;
				AutomaticModePara.ToolOutput := FALSE;
				AutomaticModePara.ToolOutputReset := FALSE;
			END_IF
			
			Communication.ToolOutput := AutomaticModePara.ToolOutput;
			
			AutomaticMode(RoboArm := RoboArm6Axis, RoboArmPara := RoboArm6AxisPara, Info := AutomaticModeInfo, Parameters := AutomaticModePara);
			
		STATE_CALIBRATION:
			
			CalibrationAction;
				
			Communication.txt_State_out  := "Calibration";
			Communication.txt_State_out_front := '$$IAT/ControlPage/txt_State_out_front.Calibration';
			
		STATE_DEMO_TILES:
			Communication.txt_State_out  := "DemoTiles";
			Communication.txt_State_out_front := '$$IAT/ControlPage/txt_State_out_front.DemoTiles';
			
			IF ControlSelector = DemoTiles AND Communication.changeModePending <> 5 THEN
				// soft exit from Demo
				IF NOT RoboArm6Axis.InMotion THEN //wait for the current program to finish execution (robot stopped)
					ControlSelector := Communication.changeModePending;
					State := STATE_READY;
				END_IF
			END_IF
			
			RoboArm6AxisPara.CoordSystem := 10;
			
			
			RoboArm6AxisPara.Position[0] := demo_move[0];
			RoboArm6AxisPara.Position[1] := demo_move[1];
			RoboArm6AxisPara.Position[2] := demo_move[2];
			RoboArm6AxisPara.Position[3] := demo_move[3];
			RoboArm6AxisPara.Position[4] := demo_move[4];
			RoboArm6AxisPara.Position[5] := demo_move[5];
			
			IF demo_move[6] > 10.0 AND NOT RoboArm6Axis.MoveActive THEN
				RoboArm6Axis.MoveDirect := TRUE;
				demo_move[6] := 0.0;
			ELSIF demo_move[6] < -10.0 AND demo_move[6] > -12.0 AND NOT RoboArm6Axis.MoveActive THEN
				RoboArm6AxisPara.ProgramName := 'aaExecuteFirst.cnc'; //this program is required for priming. change it for a textinput later.
				RoboArm6Axis.MoveProgram := TRUE;
				demo_move[6] := 0.0;
			ELSIF demo_move[6] < -99.0 AND NOT RoboArm6Axis.MoveActive THEN
				
				demo_move[6] := 0.0;
			ELSIF RoboArm6Axis.MoveDone THEN
				RoboArm6Axis.MoveDirect := FALSE;
				RoboArm6Axis.MoveProgram := FALSE;
			END_IF
			
		
		STATE_BRAKES:
			//Brakes.Cmds.BrakeOperation.SelectMode := 0;
			IF Brakes.Cmds.BrakeOperation.Error THEN
				Brakes.Info.State := brakeERROR;
			ELSE
				Brakes.Info.Error := FALSE;
			END_IF
						
			CASE Brakes.Info.State OF
				brakeERROR:
					Communication.BrakeClose := FALSE;
					Communication.BrakeOpen := FALSE;
					Communication.BrakeReadStatus := FALSE;
					Brakes.Cmds.BrakeOperation.Execute := FALSE;
					Brakes.Info.Error := TRUE;
					Brakes.Cmds.BrakeOperation();
					State := STATE_INIT;
				brakeDONE:
					Brakes.Cmds.BrakeOperation.Execute := FALSE;
					
					Brakes.Cmds.BrakeOperation();
					State := STATE_INIT;
					
				brakeOPEN:
					Communication.BrakeOpen := FALSE;
					Brakes.Cmds.BrakeOperation.AxesGroup := ADR(gAxesGroupRacer3);
					Brakes.Parameters.BrakeCmd := mcBRAKE_OPEN;
					IF Brakes.Cmds.BrakeOperation.Done AND (i = 1) THEN
						Brakes.Cmds.BrakeOperation.Execute := FALSE;
						Brakes.Cmds.BrakeOperation();
						i := 2;
					ELSIF Brakes.Cmds.BrakeOperation.Done AND (i = 2) THEN
						Brakes.Cmds.BrakeOperation.Execute := FALSE;
						Brakes.Cmds.BrakeOperation();
						i := 3;
						Communication.BrakeReadStatus := TRUE;
						Brakes.Info.State := brakeDONE;
					ELSIF (i < 3) THEN
						Brakes.Cmds.BrakeOperation.Command := Brakes.Parameters.BrakeCmd;
						Brakes.Cmds.BrakeOperation.Identifier := i;
						Brakes.Cmds.BrakeOperation.Execute := TRUE;
						
						Brakes.Cmds.BrakeOperation();
					END_IF
				
				brakeCLOSE:
					Communication.BrakeClose := FALSE;
					Brakes.Cmds.BrakeOperation.AxesGroup := ADR(gAxesGroupRacer3);
					Brakes.Parameters.BrakeCmd := mcBRAKE_CLOSE;
					IF Brakes.Cmds.BrakeOperation.Done AND (i = 1) THEN
						Brakes.Cmds.BrakeOperation.Execute := FALSE;
						Brakes.Cmds.BrakeOperation();
						i := 2;
					ELSIF Brakes.Cmds.BrakeOperation.Done AND (i = 2) THEN
						Brakes.Cmds.BrakeOperation.Execute := FALSE;
						Brakes.Cmds.BrakeOperation();
						i := 3;
						Communication.BrakeReadStatus := TRUE;
						Brakes.Info.State := brakeDONE;
					ELSIF (i < 3) THEN
						Brakes.Cmds.BrakeOperation.Command := Brakes.Parameters.BrakeCmd;
						Brakes.Cmds.BrakeOperation.Identifier := i;
						Brakes.Cmds.BrakeOperation.Execute := TRUE;
						
						Brakes.Cmds.BrakeOperation();
					END_IF
				
				brakeREAD:
					Communication.BrakeReadStatus := FALSE;
					Brakes.Parameters.BrakeCmd := mcBRAKE_GET_STATUS;
					Brakes.Cmds.BrakeOperation.AxesGroup := ADR(gAxesGroupRacer3);
					IF Brakes.Cmds.BrakeOperation.Done AND (i = 1) THEN
						Brakes.Info.BrakeStatus[i] := Brakes.Cmds.BrakeOperation.BrakeStatus;
						Brakes.Cmds.BrakeOperation.Execute := FALSE;
						Brakes.Cmds.BrakeOperation();
						i := 2;
					ELSIF Brakes.Cmds.BrakeOperation.Done AND (i = 2) THEN
						Brakes.Info.BrakeStatus[i] := Brakes.Cmds.BrakeOperation.BrakeStatus;
						Brakes.Cmds.BrakeOperation.Execute := FALSE;
						Brakes.Cmds.BrakeOperation();
						i := 3;
						Brakes.Info.State := brakeDONE;
					ELSIF (i < 3) THEN
						Brakes.Cmds.BrakeOperation.Command := Brakes.Parameters.BrakeCmd;
						Brakes.Cmds.BrakeOperation.Identifier := i;
						Brakes.Cmds.BrakeOperation.Execute := TRUE;
						
						Brakes.Cmds.BrakeOperation();
					END_IF
				
			END_CASE
		
	END_CASE
	
	RoboArm6Axis();
	
	
	////////////Visualization Variables/////////////
	VisAction;
		
		
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	RoboArm6Axis.Power := FALSE;
	RoboArm6Axis.Enable := FALSE;
	
	RoboArm6Axis(); //this will free up MpLink (possibly)
END_PROGRAM

