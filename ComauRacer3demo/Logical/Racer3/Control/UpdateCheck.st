
ACTION UpdateCheck: 
	IF Communication.CoordinateSystemSemiAuto = 0 AND SemiAutoControlPara.CoordinateSystem <> 0 THEN //Coordinate system in ManualControl and SemiAutoControl: 9 - TCS, 0 - ACS, 10 - GCS
		SemiAutoControlPara.CoordinateSystem := 0;
		SemiAutoControlPara.UpdatePending := TRUE;
	ELSIF Communication.CoordinateSystemSemiAuto = 9 AND SemiAutoControlPara.CoordinateSystem <> 9 THEN
		SemiAutoControlPara.CoordinateSystem := 9;
		SemiAutoControlPara.UpdatePending := TRUE;
	ELSIF Communication.CoordinateSystemSemiAuto = 10 AND SemiAutoControlPara.CoordinateSystem <> 10 THEN
		SemiAutoControlPara.CoordinateSystem := 10;
		SemiAutoControlPara.UpdatePending := TRUE;
	END_IF
	
	
	IF Communication.CoordinateSystemManual = 0 AND ManualControlPara.CoordinateSystem <> 0 THEN //Coordinate system in ManualControl and SemiAutoControl: 9 - TCS, 0 - ACS, 10 - GCS
		ManualControlPara.CoordinateSystem := 0;
	ELSIF Communication.CoordinateSystemManual = 9 AND ManualControlPara.CoordinateSystem <> 9 THEN
		ManualControlPara.CoordinateSystem := 9;
	ELSIF Communication.CoordinateSystemManual = 10 AND ManualControlPara.CoordinateSystem <> 10 THEN
		ManualControlPara.CoordinateSystem := 10;
	END_IF
	
	(*
	IF RoboArm6AxisPara.Distance[0] <>  SemiAutoControlPara.AskedValue.Q0 OR RoboArm6AxisPara.Distance[1] <>  SemiAutoControlPara.AskedValue.Q1 OR RoboArm6AxisPara.Distance[2] <>  SemiAutoControlPara.AskedValue.Q2 OR RoboArm6AxisPara.Distance[3] <>  SemiAutoControlPara.AskedValue.Q3 OR RoboArm6AxisPara.Distance[4] <>  SemiAutoControlPara.AskedValue.Q4 OR RoboArm6AxisPara.Distance[5] <>  SemiAutoControlPara.AskedValue.Q5  OR RoboArm6AxisPara.Position[0] <>  SemiAutoControlPara.AskedValue.Q0 OR RoboArm6AxisPara.Position[1] <>  SemiAutoControlPara.AskedValue.Q1 OR RoboArm6AxisPara.Position[2] <>  SemiAutoControlPara.AskedValue.Q2 OR RoboArm6AxisPara.Position[3] <>  SemiAutoControlPara.AskedValue.Q3 OR RoboArm6AxisPara.Position[4] <>  SemiAutoControlPara.AskedValue.Q4 OR RoboArm6AxisPara.Position[5] <>  SemiAutoControlPara.AskedValue.Q5 THEN
		SemiAutoControlPara.UpdatePending  := TRUE;
		
	END_IF
	*)
	
END_ACTION
